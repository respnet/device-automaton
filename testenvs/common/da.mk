include ../testenv-common.mk

# Do not run the observability stack by default for most local testenvs in this
# repository.
NSO_OBSERVABILITY?=false
include ../common/nso_observability.mk

# TESTID is unique per test invocation
# For example, invoking 'make test' twice in a local testenv will yield
# different TESTID. It is however stable across one test run.
ifndef TESTID
TESTID:=$(PNS)-$(shell date +%s)
endif
export TESTID

SHELL=/bin/bash

# Define the NSO container name once
export NSO_CNT=$(CNT_PREFIX)-nso

EXISTING_DEVICE ?= false

ifeq ($(NETSIM_OR_VR),netsim)
# We must use the same version of the "netsim" image as we did for our "package"
# image in /includes. We use a shell too expand the variables used in
# /includes/foo and replace string "package" with "netsim" in the tag. The env
# vars used in includes/foo (typically ${PKG_PATH}, ${NSO_VERSION} or
# ${NSO_VERSION_MM}) must be passed to the shell doing the expansion. Although
# the (make) variables are exported in nidvars.mk and nidcommon.mk, they are
# only made available as environment variables to subshells in *recipes*. The
# $(DEVICE_IMAGE) variable defined here needs an explicit export.
DEVICE_IMAGE=$(shell awk '{ print "echo", $$0 }' $(PROJECT_DIR)/includes/$(NED_REPO) | NSO_VERSION=$(NSO_VERSION) NSO_VERSION_MM=$(NSO_VERSION_MM) PKG_PATH=$(PKG_PATH) $(SHELL) | sed -e 's,/package,/netsim,')
else ifeq ($(NETSIM_OR_VR),vr)
DEVICE_IMAGE=gitlab.dev.terastrm.net:4567/terastream/vrnetlab/$(VRNETLAB_TAG)
DEVICE_CONTAINER_ARGS+= --trace
DEVICE_CONTAINER_DOCKER_ARGS+=--privileged
else
$(error NETSIM_OR_VR must be set to either "netsim" or "vr")
endif

# Is this the most horrible way of stripping the (quick-, full-) prefixes and
# (-netsim, -vr) suffixes from the testenv name? ¯\_(ツ)_/¯
PLATFORM:=$(patsubst quick-%,%,$(patsubst full-%,%,$(patsubst %-vr,%,$(patsubst %-netsim,%,$(TESTENV)))))

# Enable IPv6 per default, set to false to disable (in Makefile, not here)
IPV6?=true
# Per default, the IPv6 prefix is a randomly generated IPv6 network prefix in
# the ULA address space. Override by setting IPV6_NET variable to e.g.:
# IPV6_NET=2001:db8:1234:456:  # which becomes 2001:db8:1234:456::/64
#
# start: start the test environment in a configuration that allows
# Python Remote Debugging. Exposes port 5678 on a random port on localhost.
start:
	docker network inspect $(CNT_PREFIX) >/dev/null 2>&1 || docker network create $(CNT_PREFIX) $(shell [ "$(IPV6)" = "true" ] && export LC_ALL=C && echo --ipv6 --subnet $${IPV6_NET:-fd00:$$(< /dev/urandom tr -dc a-f0-9 | head -c4):$$(< /dev/urandom tr -dc a-f0-9 | head -c4):$$(< /dev/urandom tr -dc a-f0-9 | head -c4):}:/64)
	../docker-run.sh -td --name $(NSO_CNT) --network-alias nso $(DOCKER_NSO_ARGS) -e ADMIN_PASSWORD=NsoDocker1337 $${NSO_EXTRA_ARGS} $(IMAGE_PATH)$(PROJECT_NAME)/testnso:$(DOCKER_TAG)
	if [ "$(SKIP_PULL)" != "true" ]; then docker pull $(DEVICE_IMAGE); fi
	../docker-run.sh -td --name $(CNT_PREFIX)-dut --network-alias dut $(DOCKER_ARGS) $(DEVICE_CONTAINER_DOCKER_ARGS) $(DEVICE_IMAGE) $(DEVICE_CONTAINER_ARGS)
ifeq ($(PLATFORM),kea-yang)
	@echo "Overriding ts-dhcp-server deploy password with admin:admin"
	docker exec $(CNT_PREFIX)-dut bash -lc "useradd -m admin; echo admin:admin | chpasswd"
endif
	$(MAKE) nsobs-start
	time docker exec -t $(NSO_CNT) bash -lc 'ncs --wait-started 600'
	$(MAKE) nsobs-prepare nsobs-config-nso nsobs-print-ui-addresses

test-authgroup-service:
	$(MAKE) loadconf FILE=../common/authgroup-password.xml
	$(MAKE) runcmdJ CMD="show configuration devices authgroups group group-password" | grep default-map
	$(MAKE) runcmdJ CMD="configure\n delete devices authgroups authgroup group-password\n commit"
	$(MAKE) test-authgroup-rsa-key test-authgroup-ecdsa-key test-authgroup-ed25519-key

test-authgroup-%-key:
	$(MAKE) loadconf FILE=../common/authgroup-$*-key.xml
	$(MAKE) runcmdJ CMD="show configuration devices authgroups group group-$*-key" | grep default-map
	$(MAKE) runcmdJ CMD="show configuration ssh private-key group-$*-key" | grep key-data
	$(MAKE) runcmdJ CMD="configure\n delete devices authgroups authgroup group-$*-key\n commit"

create-device:
	-@$(MS)$(TSEQ)
# Set /devices/automaton/ned-id leaf to the current $(NED_ID). The value written
# in the leaf is in the form of "prefix:ned-id", where prefix = ned-id for most
# cases, except the NSO built-in "ned:netconf" ned-id
	PREFIX=$$([ "$(NED_ID)" == "netconf" ] && echo "ned" || echo "$(NED_ID)"); \
	mkdir -p ../common/tmp || true; xmlstarlet edit --omit-decl -N devaut=http://terastrm.net/yang/device-automaton.yang -N ncs=http://tail-f.com/ns/ncs --update /ncs:devices/devaut:automaton/devaut:ned-id --value $${PREFIX}:$(NED_ID) ../common/da-create-$(NETSIM_OR_VR).xml > ../common/tmp/da-create-$(NETSIM_OR_VR)-$(NED_ID).xml
ifeq ($(NETSIM_OR_VR),vr)
ifeq ($(PLATFORM),cisco-xr)
	@echo "Using vrnetlab credentials for $(PLATFORM)"
	xmlstarlet edit --inplace -N devaut=http://terastrm.net/yang/device-automaton.yang -N ncs=http://tail-f.com/ns/ncs \
		--delete /ncs:devices/devaut:automaton/devaut:management-endpoint/devaut:initial-credentials \
		--update /ncs:devices/devaut:automaton/devaut:management-credentials/devaut:username --value vrnetlab \
		--update /ncs:devices/devaut:automaton/devaut:management-credentials/devaut:password --value VR-netlab9 \
		--subnode /ncs:devices/devaut:automaton/devaut:management-credentials --type elem -n create-management-credentials -v "false" \
		../common/tmp/da-create-$(NETSIM_OR_VR)-$(NED_ID).xml
else ifeq ($(PLATFORM),kea-yang)
	@echo "Using admin credentials for $(PLATFORM)"
	xmlstarlet edit --inplace -N devaut=http://terastrm.net/yang/device-automaton.yang -N ncs=http://tail-f.com/ns/ncs \
		--delete /ncs:devices/devaut:automaton/devaut:management-endpoint/devaut:initial-credentials \
		--update /ncs:devices/devaut:automaton/devaut:management-credentials/devaut:username --value admin \
		--update /ncs:devices/devaut:automaton/devaut:management-credentials/devaut:password --value admin \
		../common/tmp/da-create-$(NETSIM_OR_VR)-$(NED_ID).xml
	if [ $(NSO_VERSION_MAJOR) -eq 5 ] && [ $(NSO_VERSION_MINOR) -ge 6 ] || [ $(NSO_VERSION_MAJOR) -gt 5 ]; then \
		xmlstarlet edit --inplace -N devaut=http://terastrm.net/yang/device-automaton.yang -N ncs=http://tail-f.com/ns/ncs \
			--subnode /ncs:devices/devaut:automaton --type elem -n ssh-algorithms \
			--subnode /ncs:devices/devaut:automaton/ssh-algorithms --type elem -n public-key -v "rsa-sha2-512" \
			../common/tmp/da-create-$(NETSIM_OR_VR)-$(NED_ID).xml; \
	fi
endif
endif
	$(MAKE) loadconf FILE=../common/tmp/da-create-$(NETSIM_OR_VR)-$(NED_ID).xml
	# device was added sucessfully (timeout 900s). initial create time is
	# intentionally high because the virtual router may take some time to boot
	if [ "$(NETSIM_OR_VR)" = "vr" ]; then time_limit=900; else time_limit=30; fi; \
	../cmd.py 'show devices automaton dut plan component self state ready status' --success-pattern 'status reached' \
		--retry --time-limit $${time_limit} --on-fail 'show devices automaton dut plan | tab\nshow al:alarms'
	# show the ned-id detection results
	$(MAKE) runcmdJ CMD="show devices automaton dut auto-ned-id"
ifneq ($(PLATFORM),cisco-xr)
ifeq ($(PLATFORM),cisco-xr-7.6)
# Extra sync-from after device is onboarded to ensure the "magic" configuration
# for initial user (vrnetlab) is removed too.
	$(MAKE) runcmdJ CMD="request devices device dut sync-from"
endif
ifeq ($(NETSIM_OR_VR),vr)
	# make sure the initial user (vrnetlab) was removed
	! $(MAKE) runcmdJ CMD="show configuration devices device dut config" | grep vrnetlab
endif
endif
	-@$(ME)$(TSEQ)

delete-device:
	-@$(MS)$(TSEQ)
	$(MAKE) runcmdJ CMD="configure\n delete devices automaton dut\n commit"
	# If there are commit queue items in processing, the current implementation
	# pauses for processing to complete. The timeout may take upwards of "write-timeout" (20s)
	# TODO: revisit this when commit-queue handling is defined
	../cmd.py 'show configuration devices device' --success-pattern 'No entries found' \
		--retry --time-limit 30 --on-fail 'show configuration devices device\nshow al:alarms'
	@echo "Verifying the authgroup service is removed"
	$(MAKE) runcmdJ CMD="show configuration devices authgroups authgroup" | grep "No entries found"
	@echo "Verifying alarms for the device are cleared"
	$(MAKE) runcmdJ CMD="show al:alarms alarm-list alarm dut is-cleared false" | grep "No entries found"
	-@$(ME)$(TSEQ)

test-device-automaton:
	$(MAKE) create-device
	$(MAKE) delete-device

.PHONY: start test-device-automaton create-device delete-device

