include ../common/da.mk

test:
	@echo "\n== Running quick tests (\$$(EXISTING_DEVICE)=$(EXISTING_DEVICE))"
	$(MAKE) test-authgroup-service
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) test-device-automaton
else
	$(MAKE) create-device
endif

.PHONY: test
