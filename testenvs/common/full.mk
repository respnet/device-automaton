include ../common/da.mk

test:
	@echo "\n== Running full tests (\$$(EXISTING_DEVICE)=$(EXISTING_DEVICE))"
	$(MAKE) test-authgroup-service
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) test-device-automaton
else
	$(MAKE) create-device
endif
	$(MAKE) test-device-monitor
	$(MAKE) test-alarm-connectivity
	$(MAKE) test-alarm-authentication
	$(MAKE) test-alarm-handle-commit-queue
ifeq ($(NETSIM_OR_VR),vr)
# Until I figure out how to change credentials on a netsim device, just execute
# these on virtual routers
	$(MAKE) test-change-ssh-private-key
	$(MAKE) test-change-ssh-password
endif
# WARNING: this test effectively wipes service-meta-data (refcounts & backpointers).
	$(MAKE) test-alarm-check-sync

test-device-monitor:
	-@$(MS)$(TSEQ)
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) create-device
endif
	# make it fail
	docker network disconnect $(CNT_PREFIX) $(CNT_PREFIX)-dut
	../cmd.py 'show devices automaton dut plan component self state ready status' --success-pattern 'status failed' \
		--retry --on-fail 'show devices automaton dut plan | tab\nshow al:alarms'
	# make sure alarm was raised
	../cmd.py 'show al:alarms alarm-list alarm dut management-endpoint-unreachable is-cleared | notab' --success-pattern 'is-cleared false' \
		--retry --time-limit 20 --on-fail 'show al:alarms'
	../cmd.py 'show al:alarms alarm-list alarm dut device-unreachable is-cleared | notab' --success-pattern 'is-cleared false' \
		--retry --time-limit 20 --on-fail 'show al:alarms'

	# make it work again
	docker network connect --alias dut $(CNT_PREFIX) $(CNT_PREFIX)-dut
	../cmd.py 'show devices automaton dut plan component self state ready status' --success-pattern 'status reached' -t 500 \
		--retry --on-fail 'show devices automaton dut plan | tab\nshow al:alarms'
	# make sure alarm was cleared
	../cmd.py 'show al:alarms alarm-list alarm dut management-endpoint-unreachable is-cleared | notab' --success-pattern 'is-cleared true' \
		--retry --time-limit 20 --on-fail 'show al:alarms'
	../cmd.py 'show al:alarms alarm-list alarm dut device-unreachable is-cleared | notab' --success-pattern 'is-cleared true' \
		--retry --time-limit 20 --on-fail 'show al:alarms'

	# show final plan
	../cmd.py 'show devices automaton dut plan | tab'

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) delete-device
endif
	-@$(ME)$(TSEQ)

# Use this target to get a count of 'Reacted to $ALARM' messages in the service
# log. The value for the $ALARM variable is extracted from the target name.
get-%-alarm-count: ALARM=$(subst get-,,$(subst -alarm-count,,$@))
get-%-alarm-count:
	@$(MAKE) runcmdJ CMD='show devices automaton dut log log-entry message | match \"Reacted to al:${ALARM} alarm\" | count' | awk '{ print $$2 }'

test-alarm-connectivity:
	-@$(MS)$(TSEQ)
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) create-device
endif
# First we must prevent the device monitor from fixing the address in the background :)
	../cmd.py 'configure\nset devices global-settings automaton device-monitor disabled\ncommit and-quit'
# Device monitor only checks whether it should run at the beginning of its
# cycle. To ensure the setting is read immediately just restart.
	../cmd.py 'request packages package device-automaton redeploy'
# This is all a "single" shell script because we need to remember the count of
# service log messages before starting the test. The value is then checked
# again at the end and it must increase.
	set -e; \
	CURRENT_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-connection-failure-alarm-count); \
	# fake conection failure by changing the IP address of the device in NSO and issuing connect
	../cmd.py 'configure\nset devices device dut address 1.2.3.4\ncommit and-quit'; \
	../cmd.py 'request devices device dut connect' --success-pattern 'Failed to connect to device dut|Connection to dut timed out|Failed to establish connection' \
		--retry --time-limit 30; \

	# ensure device automaton reacts to alarm and sorts things out
	../cmd.py 'request devices device dut connect' --success-pattern 'result true' --retry --time-limit 30; \
	../cmd.py 'show devices automaton dut plan component self state ready status' --success-pattern 'status reached' \
		--retry --time-limit 30 --on-fail 'show devices automaton dut plan | tab\nshow devices automaton dut log | tab\nshow al:alarms'; \
	NEW_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-connection-failure-alarm-count); \
	[[ $${NEW_ALARM_COUNT} -gt $${CURRENT_ALARM_COUNT} ]] || exit 1
	../cmd.py 'configure\nset devices global-settings automaton device-monitor enabled\ncommit and-quit'

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) delete-device
endif
	-@$(ME)$(TSEQ)

test-alarm-authentication:
	-@$(MS)$(TSEQ)
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) create-device
endif
# This is all a "single" shell script because we need to remember the count of
# service log messages before starting the test. The value is then checked
# again at the end and it must increase.
	set -e; \
	CURRENT_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-connection-failure-alarm-count); \
	# disconnect to avoid cached connections
	../cmd.py 'request devices device dut disconnect'; \
	# sleep to avoid xrv rate limit
	sleep 5; \
	# fake connection failure by changing authentication credentials of the device in NSO and issuing connect
	../cmd.py 'configure\nset devices authgroups group dut default-map remote-name foo\ncommit and-quit'; \
	../cmd.py 'request devices device dut connect' --success-pattern 'result false' --retry --time-limit 30; \

	# ensure device automaton reacts to alarm and sorts things out
	../cmd.py 'request devices device dut connect' --success-pattern 'result true' --retry --time-limit 240; \
	../cmd.py 'show devices automaton dut plan component self state ready status' --success-pattern 'status reached' \
		--retry --time-limit 30 --on-fail 'show devices automaton dut plan | tab\nshow devices automaton dut log | tab\nshow al:alarms'; \
	NEW_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-connection-failure-alarm-count); \
	[[ $${NEW_ALARM_COUNT} -gt $${CURRENT_ALARM_COUNT} ]] || exit 1

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) delete-device
endif
	-@$(ME)$(TSEQ)

test-alarm-check-sync:
	-@$(MS)$(TSEQ)
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) create-device
endif
# This is all a "single" shell script because we need to remember the count of
# service log messages before starting the test. The value is then checked
# again at the end and it must increase.
	set -e; \
	CURRENT_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-out-of-sync-alarm-count); \
	# fake out-of-sync by removing the configuration on the device in NSO, commit
	# with no-networking, thus leaving NSO with a different config (no config)
	# than what is on the device.. and finally issue check-sync to let NSO find
	# out about the out-of-sync situation and raise an alarm
	../cmd.py 'configure\ndelete devices device dut config\ncommit no-networking and-quit' --retry --time-limit 30; \
	../cmd.py 'request devices automaton dut device-check-sync' --success-pattern 'out-of-sync'; \

	# ensure device automaton reacts to alarm and sorts things out
	../cmd.py 'request devices device dut check-sync' --success-pattern 'result in-sync' --retry --time-limit 240; \
	../cmd.py 'show devices automaton dut plan component self state ready status' --success-pattern 'status reached' \
		--retry --time-limit 30 --on-fail 'show devices automaton dut plan | tab\nshow devices automaton dut log | tab\nshow al:alarms'; \
	NEW_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-out-of-sync-alarm-count); \
	[[ $${NEW_ALARM_COUNT} -gt $${CURRENT_ALARM_COUNT} ]] || exit 1
	../cmd.py 'request devices automaton dut device-check-sync' --success-pattern 'in-sync'

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) delete-device
endif
	-@$(ME)$(TSEQ)

test-alarm-handle-commit-queue:
	-@$(MS)$(TSEQ)
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) create-device
endif
# First we must prevent the configuration consistency guarantor from fixing the
# out-of-sync state in the background and disable automatic reaction to
# out-of-sync alarm
	../cmd.py 'configure\nset devices automaton dut alarm-reaction out-of-sync false\ncommit and-quit'
# Wait for a bit until the work function (triggered by the change above)
# completes its run. If we proceed immediately there is a chance of the work
# function "fixing" the out-of-sync situation artificially created later in this
# test :)
	../cmd.py 'show devices automaton dut work-count' --success-pattern 'work-count 0' --retry --time-limit 60
	../cmd.py 'configure\nset devices global-settings automaton configuration-consistency-guarantor disabled\ncommit and-quit'
# This is all a "single" shell script because we need to remember the count of
# service log messages before starting the test. The value is then checked
# again at the end and it must increase.
	set -e; \
	CURRENT_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-commit-through-queue-failed-alarm-count); \
# Ensure we get a non-transient error when committing the change via the commit
# queue. the first (no-networking) commit introduces an out-of-sync change which
# is detected in the second commit
	../cmd.py 'configure\nset devices device dut user-account test password test123\ncommit no-networking'; \
# We retry this operation because a device may be running an exclusive commit
# queue item - "block-others". These are created internally by NSO for sync-*
# operations.
	../cmd.py 'configure\nset devices device dut user-account test password newpass123\ncommit' --retry --time-limit 30 --on-fail 'show devices commit-queue'; \

	../cmd.py 'request devices automaton dut device-check-sync' --success-pattern 'result in-sync' --retry --time-limit 30 --on-fail 'show devices automaton dut log'; \
# Restore settings to their default values
	../cmd.py 'configure\nset devices global-settings automaton configuration-consistency-guarantor enabled\ncommit and-quit'; \
	../cmd.py 'configure\ndelete devices automaton dut alarm-reaction out-of-sync\ncommit and-quit'; \
	../cmd.py 'configure\ndelete devices device dut user-account test\ncommit and-quit'; \
# Ensure device automaton reacted to correct alarm
	NEW_ALARM_COUNT=$$($(MAKE) --silent --no-print-directory get-commit-through-queue-failed-alarm-count); \
	[[ $${NEW_ALARM_COUNT} -gt $${CURRENT_ALARM_COUNT} ]] || exit 1

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) delete-device
endif
	-@$(ME)$(TSEQ)

test-change-ssh-private-key:
	$(MAKE) test-change-rsa-private-key
	$(MAKE) test-change-ecdsa-private-key
ifneq (,$(findstring alu-sr,$(PLATFORM)))
	echo "ED25519 SSH key types not supported on SROS?!"
else
	$(MAKE) test-change-ed25519-private-key
endif

test-change-%-private-key:
#	-@$(MS)$(TSEQ)
ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) create-device
endif

	# use SSH key for device-specific managment credentials
	$(MAKE) loadconf FILE=../common/da-ssh-$*-key.xml

	# check device-specific authgroup
	../cmd.py 'show configuration devices authgroups group dut default-map public-key private-key' --success-pattern dut \
		--time-limit 30 --retry --on-fail 'show configuration devices authgroups group dut'

	# sync to make sure new credentials work
	../cmd.py 'request devices device dut sync-from' --success-pattern 'result true' \
		--time-limit 30 --retry

ifneq (,$(findstring alu-sr,$(PLATFORM)))
	echo "Skipping SSH password credentials removed test on SROS???"
else
	# attempt to use the password again (should have been removed)
	../cmd.py 'configure\nset devices authgroups group dut default-map remote-password Local-management1\ncommit and-quit'

	# connect to make sure the password doesn't work
	../cmd.py 'request devices device dut connect' --success-pattern 'Failed to (authenticate|connect|establish connection)'

	# wait for the device automaton to react to alarm and restore the augthroup
	../cmd.py 'show configuration devices authgroups group dut default-map public-key private-key' --success-pattern dut \
		--time-limit 30 --retry --on-fail 'show configuration devices authgroups group dut'
endif
	# restore password access (key is removed automatically by "choice")
	../cmd.py 'configure\nset devices automaton dut management-credentials password Local-management1\ncommit and-quit'
	# check device-specific authgroup to make sure password is now used
	../cmd.py 'show configuration devices authgroups group dut default-map' --success-pattern password \
		--time-limit 60 --retry --on-fail 'show configuration devices authgroups group dut'

	# sync to make sure new credentials work
	../cmd.py 'request devices device dut sync-from' --success-pattern 'result true' \
		--time-limit 30 --retry

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) delete-device
endif
#	-@$(ME)$(TSEQ)

test-change-ssh-password:
	-@$(MS)$(TSEQ)
# This test will cycle the device automaton through a couple of credential changes and keep checking if the device still works
# 1. Device automaton starts with the device-specific `managed-admin` user.
# 2. Change device-specific credentials to trigger reconfiguration of this device and restore the initial state.

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) create-device
endif

	# change device management credentials (original managed-admin/Local-management1)
	../cmd.py 'configure\nset devices automaton dut management-credentials username device-management password Local-management2\ncommit and-quit'

	# check device-specific authgroup
	../cmd.py 'show configuration devices authgroups group dut default-map remote-name' --success-pattern device-management \
		--time-limit 30 --retry --on-fail 'show configuration devices authgroups group dut'

	# sync to make sure new credentials work
	../cmd.py 'request devices device dut sync-from' --success-pattern 'result true' \
		--time-limit 120 --retry

	# restore device-specific management credentials
	../cmd.py 'configure\nset devices automaton dut management-credentials username managed-admin password Local-management1\ncommit and-quit'

	# check device-specific authgroup
	../cmd.py 'show configuration devices authgroups group dut default-map remote-name' --success-pattern managed-admin \
		--time-limit 120 --retry --on-fail 'show configuration devices authgroups group dut'

	# sync to make sure new credentials work
	../cmd.py 'request devices device dut sync-from' --success-pattern 'result true' \
		--time-limit 120 --retry

ifneq ($(EXISTING_DEVICE),true)
	$(MAKE) delete-device
endif
	-@$(ME)$(TSEQ)


.PHONY: test test-device-automaton test-device-monitor test-alarm-connectivity test-alarm-authentication test-alarm-check-sync test-alarm-handle-commit-queue create-device delete-device test-change-ssh-private-key test-change-ssh-password
