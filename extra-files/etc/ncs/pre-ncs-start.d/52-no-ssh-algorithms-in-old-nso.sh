#!/bin/bash

if [ ${MANGLE_CONFIG} = "false" ]; then
    echo "Config mangling disabled, early exit..."
    exit
fi

CONF_FILE=${CONF_FILE:-/etc/ncs/ncs.conf}

# Determine NSO version with major / minor version component
if [[ $(ncs --version) =~ ^([0-9]+)\.([0-9]+) ]]; then
    NSO_VERSION_MAJOR=${BASH_REMATCH[1]}
    NSO_VERSION_MINOR=${BASH_REMATCH[2]}
else
    echo "Not a proper NSO version"
    exit 1
fi

if [[ ${NSO_VERSION_MAJOR} -eq 5 && ${NSO_VERSION_MINOR} -lt 6 || ${NSO_VERSION_MAJOR} -lt 5 ]]; then
    rm /nso/run/cdb/ssh-algorithms.xml
fi
