#!/bin/bash

if [ ${MANGLE_CONFIG} = "false" ]; then
    echo "Config mangling disabled, early exit..."
    exit
fi

CONF_FILE=${CONF_FILE:-/etc/ncs/ncs.conf}

# Change level for devel.log to trace (from info)
xmlstarlet edit --inplace -N x=http://tail-f.com/yang/tailf-ncs-config \
           --update '/x:ncs-config/x:logs/x:developer-log-level' --value "trace" \
           $CONF_FILE
