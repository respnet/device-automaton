include nidvars.mk

# The default testenv should be a "quick" testenv, containing a simple test that
# is quick to run.
DEFAULT_TESTENV?=quick-alu-sr-netsim

# Include standard NID (NSO in Docker) package Makefile that defines all
# standard make targets
include nidpackage.mk

push-testnso:
	docker push $(IMAGE_PATH)$(PROJECT_NAME)/testnso:$(DOCKER_TAG)

pull-package:
	docker pull $(IMAGE_PATH)$(PROJECT_NAME)/package:$(DOCKER_TAG)
	docker pull $(IMAGE_PATH)$(PROJECT_NAME)/package:MM_$(DOCKER_TAG_MM)

generate-test-jobs:
	nid/generate-test-jobs > test-jobs.yaml

# Run a command in a dev container. Note how this doesn't work in CI as it uses
# a bind mount.
DEVRUN=docker run -t --rm -v $$(pwd):/src $(NSO_IMAGE_PATH)cisco-nso-dev:$(NSO_VERSION) bash -lc

test-packages/dummy-ned-1.0:
	mkdir -p test-packages
	$(DEVRUN) 'cd /src/test-packages && ncs-make-package --netconf-ned ../dummy-v1 --package-version 1.0 --no-java --no-python dummy-ned-1.0 && chown -R $(shell id -u):$(shell id -g) dummy-ned-1.0 && xmlstarlet edit --inplace -N x=http://tail-f.com/ns/ncs-packages --update "/x:ncs-package/x:ncs-min-version" --value 4.7 --update "/x:ncs-package/x:name" --value dummy-ned dummy-ned-1.0/src/package-meta-data.xml.in'
	sed -i -e '/^\s\+--fail-on-warnings/d' -e 's/ --fail-on-warnings//' $@/src/Makefile
	sed -i -e '/<AES256CFB128>/,+2d' $@/netsim/confd.conf.netsim

test-packages/dummy-ned-2.0:
	mkdir -p test-packages
	$(DEVRUN) 'cd /src/test-packages && ncs-make-package --netconf-ned ../dummy-v1 --package-version 2.0 --no-java --no-python dummy-ned-2.0 && chown -R $(shell id -u):$(shell id -g) dummy-ned-2.0 && xmlstarlet edit --inplace -N x=http://tail-f.com/ns/ncs-packages --update "/x:ncs-package/x:ncs-min-version" --value 4.7 --update "/x:ncs-package/x:name" --value dummy-ned dummy-ned-2.0/src/package-meta-data.xml.in'
	sed -i -e '/^\s\+--fail-on-warnings/d' -e 's/ --fail-on-warnings//' $@/src/Makefile
	sed -i -e '/<AES256CFB128>/,+2d' $@/netsim/confd.conf.netsim

test-packages/dummy-ned-3.0:
	mkdir -p test-packages
	$(DEVRUN) 'cd /src/test-packages && ncs-make-package --netconf-ned ../dummy-v2 --package-version 3.0 --no-java --no-python dummy-ned-3.0 && chown -R $(shell id -u):$(shell id -g) dummy-ned-3.0 && xmlstarlet edit --inplace -N x=http://tail-f.com/ns/ncs-packages --update "/x:ncs-package/x:ncs-min-version" --value 4.7 --update "/x:ncs-package/x:name" --value dummy-ned dummy-ned-3.0/src/package-meta-data.xml.in'
	sed -i -e '/^\s\+--fail-on-warnings/d' -e 's/ --fail-on-warnings//' $@/src/Makefile
	sed -i -e '/<AES256CFB128>/,+2d' $@/netsim/confd.conf.netsim

.PHONY: test-packages/dummy-ned-1.0 test-packages/dummy-ned-2.0 test-packages/dummy-ned-3.0
