<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:config="http://tail-f.com/ns/config/1.0"
    xmlns:ncs="http://tail-f.com/ns/ncs"
    $XMLNS
>
<!-- xmlns:ned_{idx}="{ned}" separated with newlines -->
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="config:* | ncs:* | @* | node()">
        <xsl:copy>
          <xsl:apply-templates select="@* | node()" />
        </xsl:copy>
    </xsl:template>

<!-- ned_{idx}:* joined with | -->
    <xsl:template match="$MATCH">
        <xsl:copy>
          <xsl:apply-templates select="@* | node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="*">
        <xsl:comment>Removed in template preprocessing because the namespace (NED) was not found at build time</xsl:comment>
    </xsl:template>
</xsl:stylesheet>
