# All Python modules
PYTHON_MODULES = $(wildcard ../python/*)
# All Python files
PYTHON_FILES = $(shell find ../python -name "*.py")

# get list of required packages
REQ_PKGS=$(strip $(shell xmlstarlet sel -N pkg="http://tail-f.com/ns/ncs-packages" -t  -m '//pkg:required-package' -v . ../package-meta-data.xml))
REQ_PKGS_PATH=$(addsuffix /python, $(addprefix ../../, $(REQ_PKGS)))

# convert REQ_PKGS_PATH into colon separated list
EXTRA_PYTHON_PATH=$(shell echo $(REQ_PKGS_PATH) | tr ' ' :)

# Add required packages to PYTHONPATH
export PYTHONPATH :=$(EXTRA_PYTHON_PATH):$(PYTHONPATH)
# This indirect invocation will use packages in an activated virtualenv.
# Directly calling pylint would not.
PYLINT = python3 $(shell which pylint) --ignore=namespaces --rcfile .pylintrc
MYPY = python3 $(shell which mypy)

# run pylint and check return code
#    Pylint should leave with following status code:
#   * 0 if everything went fine
#   * 1 if some fatal message issued
#   * 2 if some error message issued
#   * 4 if some warning message issued
#   * 8 if some refactor message issued
#   * 16 if some convention message issued
#   * 32 on usage error
#   status 1 to 16 will be bit-ORed so you can know which different
#   categories has been issued by analysing pylint output status code
# we only care about errors or fatal messages, thus checking for 1 or 2

# Our pylint targets, i.e. the output directories of pylint for our modules (if
# there's more than one)
PYLINT_TARGETS=$(addprefix .pylint.d/,$(addsuffix 1.stats,$(subst ../python/,,$(PYTHON_MODULES))))
# Just a conveniently named target that depends on the pylint output directories
# for all Python modules
pylint: $(PYLINT_TARGETS)

# Depending on all Python files mean we will only run pylint if any of the
# Python files are newer than the pylint report, i.e. anything has actually
# changed. Unfortunately, defining the dependencies per python module is really
# hard, so any change in any module will lead to running pylint on all of them.
PY_MODULE=$(subst .pylint.d/,,$(subst 1.stats,,$@))
.pylint.d/%1.stats: $(PYTHON_FILES)
	PYLINTHOME=.pylint.d $(PYLINT) --ignore=namespaces ../python/$(PY_MODULE); RET=$$?; echo "Pylint return code: $$RET"; if [ $$((($$RET&1)==1 || ($$RET&2)==2)) -eq 1 ]; then echo "Pylint fatal/error"; exit 1; else echo "Pylint OK"; fi

mypy: export MYPYPATH=../python/stubs:$(EXTRA_PYTHON_PATH)
mypy: export MYPY_FORCE_COLOR=1
mypy:
	$(MYPY) --check-untyped-defs --ignore-missing-imports $(PYTHON_MODULES)

clean-lint: clean-pylint clean-mypy clean-pycache

clean-pylint:
	rm -fr .pylint.d

clean-mypy:
	rm -fr .mypy_cache

clean-pycache:
	-find ../python -name __pycache__ -exec rm -rf {} \;
