import re
import time
import traceback
from typing import Union

from typing_extensions import Protocol

import ncs

from . import utils
from .. import ha_sub


def device_automaton_removed(e: Exception) -> bool:
    service_removed = re.search(r'/ncs:devices/devaut:automaton\{[^}]+\} does not exist', str(e)) is not None or \
        re.search(r'\{[^}]+\} not in /ncs:devices/devaut:automaton', str(e)) is not None
    # This kind of error is raised when we attempt to "move" the plan states on a non-existing automaton instance
    plan_removed = re.search(r'item does not exist \(1\): /ncs:devices/devaut:automaton\{[^}]+\}/[^:]+', str(e)) is not None
    return service_removed or plan_removed


def device_entry_removed(e: Exception) -> bool:
    return re.search(r'/ncs:devices/device\{[^}]+\} does not exist', str(e)) is not None or \
        re.search(r'\{[^}]+\} not in /ncs:devices/device', str(e)) is not None


class LoggerWrapper:
    def __init__(self, log, caller):
        self.log = log
        self.caller = caller

    def debug(self, msg, *args, **kwargs):
        self.log.debug('{}: {}'.format(self.caller, msg), *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.log.info('{}: {}'.format(self.caller, msg), *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self.log.warning('{}: {}'.format(self.caller, msg), *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.log.error('{}: {}'.format(self.caller, msg), *args, **kwargs)

    def exception(self, msg, *args, **kwargs):
        self.log.exception('{}: {}'.format(self.caller, msg), *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.log.critical('{}: {}'.format(self.caller, msg), *args, **kwargs)


class SupportsLogging(Protocol):
    log: Union[ncs.log.Log, LoggerWrapper]


class SupportsStartStop(Protocol):
    """A protocol definition for a standard worker, supporting start() and stop() methods."""
    def start(self) -> None:
        ...

    def stop(self) -> None:
        ...


class DeviceTimeoutTaker(SupportsLogging):
    """Mixin class to add internal _take_timeout method wherever multiple SSH connections are made"""
    def _take_timeout(self, device, node_or_trans):
        """Helper method for working around Cisco XR device with 'leaky bucket as a queue' SSH connection rate limiter.

        :param device: device name
        :param node_or_trans: ncs.maagic.Node or ncs.maapi.Transaction, used to read device type"""
        if not isinstance(node_or_trans, (ncs.maagic.Node, ncs.maapi.Transaction)):
            raise ValueError('node_or_trans type not ncs.maagic.Node or ncs.maapi.Transaction, but ' + str(type(node_or_trans)))
        read_root = ncs.maagic.get_root(ncs.maagic.get_trans(node_or_trans))
        try:
            needs_timeout = 'http://cisco.com/ns/yang/cisco-xr-types' in read_root.ncs__devices.ncs__device[device].capability
        except ValueError:
            # Reading the device OS may fail because we have not connected
            # to the device yet. Let's err on the side of caution and take
            # a timeout anyway.
            needs_timeout = True
        if needs_timeout:
            time.sleep(1)


class Retrier(SupportsLogging):
    """Mixin class to add internal _retry_function method"""

    def _retry_function(self, retry_fn, result_eval_fn=lambda _: (True, None),
                        args=None, max_retries=5, pause=3, suffix=None):
        """A retry wrapper for a given function.

        :param retry_fn: function to retry, must return something
        :param result_eval_fn: result evaluation function, must return tuple of (success, message),
            where success is Truthy/Falsy, and message any string
        :param args: optional list of positional args passed to retry_fn
        :param max_retries: maximum number of retriers for a failed attempt
        :param pause: timeout between retries
        :param suffix: log message suffix
        :return: final result (successful or unsuccessful)"""
        if args is None:
            args = []
        retry_count = 1
        while retry_count <= max_retries:
            if retry_count > 1:
                time.sleep(pause)
            self.log.debug('{}/{} {}'.format(retry_count, max_retries, suffix or ''))
            try:
                result = result_eval_fn(retry_fn(*args))
            except Exception as e:
                if device_automaton_removed(e) or device_entry_removed(e):
                    self.log.warning('{kp}: device is in the process of being removed, aborting retry attempts')
                    return (False, e)
                else:
                    self.log.error('{}/{} {} error: {}'.format(retry_count, max_retries, suffix or '', e))
                    self.log.error(traceback.format_exc())
                    retry_count += 1
                    result = (False, e)
            else:
                if result[0]:
                    break
                else:
                    self.log.debug('{}/{} {} result: {}'.format(retry_count, max_retries, suffix or '', result[1]))
                    retry_count += 1
        return result


class SubscriberIterSkipSlave(SupportsLogging):
    """Mixin class for subscriber iter classes. Defines should_iterate() that skips on HA slaves."""
    def should_iterate(self) -> bool:
        if ha_sub.is_ha_master_or_no_ha():
            return True
        else:
            self.log.debug("We are not master or not in non-HA mode, will not iterate")
            return False


class DeviceCapability:
    """Helper for checking YANG module support in device capabilities and also NED

    NETCONF devices self-report their supported YANG modules as device
    capabilities. Just checking the list of capabilities is not sufficient
    criteria for applying device configuration. The associated NED must also
    contain the same module."""
    def __init__(self, device: ncs.maagic.Container):
        self.device = device

    def check_capability(self, capability: str) -> bool:
        """Check if YANG module (namespace) is present in device capabilities and NED"""
        if capability not in self.device.capability:
            return False
        ned_id = utils.read_ned_id(self.device)
        root = self.device._parent._parent._parent
        ned_module_list = root.ncs__devices.ned_ids.ned_id[ned_id].module
        for _ in ned_module_list.filter(f'namespace="{capability}"'):
            return True
        else:
            return False
