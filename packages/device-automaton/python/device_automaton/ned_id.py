"""The ned_id proof of concept API

This module contains a proof of concept implementation of NSO Python API for
working with YANG identities. In YANG, an identity is a unique identifier that
is either defined from scratch or is derived from one or more base identities.
This is used in different parts of NSO and external packages. One of the more
prominent use cases are ned-ids - the hierarchy of NED identifiers that include
the numeric version. In the automaton package we support configuring a concrete
(non-abstract) identity like one would normally use when adding a device in
NSO. In addition we support configuring an abstract identity representing a
family of NEDs - all ned-ids derived from a base identity.

The 'Identity' class is the generic wrapper for any YANG identities. The
'NedInfo' class contains additional function to discover more information about
identities used for ned-id.

Right now NSO lacks a Python API (or MAAPI function call) for discovering the
schema information about identities. Really the only bit of information we get
is the fully qualified identity name when reading a leaf with of type
identityref. XPath has two functions: "derived-from()" and
"derived-from-or-self(), but we can't use those in code.

We officially requested the new feature from Cisco, but it will likely take
time to implement and even then it will likely only work in the latest
version. For the time being, this hacky implementation is the best we got :)
"""
import re
from dataclasses import dataclass
from itertools import groupby
from typing import List, Optional, Set, Dict

import ncs


def _get_base_identity(identity: str) -> Set[str]:
    """Placeholder for missing NSO identity API - get base identity

    In YANG an identity may be derived from more than one base directly. Hence
    the return structure being a set."""
    # All the NED identities I have seen out there follow a very predictable
    # pattern. The fully qualified identity consists of a prefix (YANG module)
    # and the identifier. The prefix and identifier are the same string in NEDs
    # maintained by Tail-f. For example, 'alu-sr-cli-8.14:alu-sr-cli-8.14' is
    # defined in a YANG module with prefix 'alu-sr-cli-8.14'. These are
    # automatically generated at build time and the same pattern is used in the
    # NED package skeletons.
    # The only exceptions to this rule I found are the built-in identities.
    # They are all defined in the tailf-ncs-ned YANG module with the prefix 'ned'.
    # Caveat emptor: if the code does not work for your identities, bear in
    # mind the use case here - discovering the identity hierarchy for
    # *ned-ids*, not any generic identities.
    if identity in ('ned:netconf-ned-id', 'ned:cli-ned-id', 'ned:generic-ned-id', 'ned:snmp-ned-id'):
        return set(('ned:ned-id',))

    if identity.endswith('-nc') or identity in ('ned:netconf', 'ned:lsa-netconf', 'ned:rfc5277-id'):
        return set(('ned:netconf-ned-id',))
    elif identity.endswith('-cli'):
        return set(('ned:cli-ned-id',))
    elif identity.endswith('-gen'):
        return set(('ned:generic-ned-id',))
    elif identity == 'ned:snmp':
        return set(('ned:snmp-ned-id',))
    elif identity == 'ned:ned-id':
        return set()
    elif re.search(r'-\d+(\.\d+)?$', identity):
        split = identity.split(':')
        if split[0] != split[1]:
            raise ValueError(f'Unexpected identity where prefix != identity: {identity}')
        stripped = ':'.join(re.sub(r'-\d+(\.\d+)?$', '', part) for part in split)
        return set((stripped,))
    else:
        # Add an rule for the unknown format above
        raise ValueError(f'Unknown identity format for ned-id: {identity}')


def _derived_from(base: str, identity: str, or_self: bool = False) -> bool:
    """Placeholder for missing NSO identity API - get base identity

    The behavior of this function is similar to the XPath
    "derived-from(-or-self)" function."""
    if or_self and base == identity:
        return True
    my_base = _get_base_identity(identity)
    if base in my_base:
        return True
    return any(_derived_from(base, b, or_self) for b in my_base)


@dataclass(frozen=True)
class Identity:
    """Wrapper class for NSO Python identity API

    This class presents the information about any YANG identity. In future the
    implementation could be replaced with MAAPI calls.
    Example use for an identity 'some-identity' derived from
    'parent-identity':

      identity = Identity.from_cdb('prefix:some-identity')
      print(identity.base) # prints prefix:base-identity

    """

    identity: str
    abstract: bool = False

    @classmethod
    def from_cdb(cls, identity: str) -> 'Identity':
        """The "constructor" for the Identity class

        This method takes the raw identity name as written in NSO CDB and
        creates the Identity object."""
        # With the real API we could probably look up the abstract statment
        # (schema info??). Right now we make the general assumption that any
        # ned-id ending with a numeric version identifier is non-abstract. In
        # addition we list some exceptions for known non-abstract ned-ids seen
        # in our environment.
        if re.search(r'-\d+(\.\d+)?$', identity) is not None:
            abstract = False
        elif identity == ('ned:lsa-netconf'):
            abstract = False
        else:
            abstract = True
        return Identity(identity, abstract)

    @property
    def base(self) -> Set['Identity']:
        return {Identity.from_cdb(b) for b in _get_base_identity(self.identity)}

    def derived_from(self, base: 'Identity', or_self: bool = False) -> bool:
        return _derived_from(base.identity, self.identity, or_self)

    def __str__(self):
        return self.identity


@dataclass(frozen=True)
class NedInfo:
    """The NedInfo class wraps Identity and adds additional functions

    The ned-id identities carry additional information apart from the identity
    name itself."""
    identity: Identity
    family: Identity

    @property
    def netconf(self):
        return _derived_from('ned:netconf-ned-id', self.family.identity, or_self=True)

    @property
    def generic(self):
        return _derived_from('ned:generic-ned-id', self.family.identity, or_self=True)


    @classmethod
    def from_cdb(cls, identity: str) -> 'NedInfo':
        id_info = Identity.from_cdb(identity)
        # If we're looking at a non-abstract ned-id (including version) then
        # its family is the immediate base identity. For example ned-id
        # alu-sr-cli-8.14 belongs to the family alu-sr-cli. If the ned-id is
        # abstract, then it already represents the family!
        if id_info.abstract is False:
            # ned-ids always have a single base
            if len(id_info.base) != 1:
                raise ValueError(f'Unexpected number of base identities: {id_info.base}')
            family = id_info.base.pop()
        else:
            family = id_info
        return NedInfo(id_info, family)

    @property
    def version(self) -> Optional[str]:
        if self.identity.abstract is True:
            return None
        m = re.search(r'-(\d+(\.\d+)?)', str(self.identity))
        if m is None:
            return None
        return m.group(1)

    def __lt__(self, other):
        try:
            v1 = self.version.split('.') # type: ignore
        except TypeError:
            v1 = str(self.identity)
        try:
            v2 = other.version.split('.')
        except TypeError:
            v2 = str(other.identity)

        try:
            return v1 < v2
        except TypeError:
            return v1 is None


def get_loaded_neds(root: ncs.maagic.Root) -> List[NedInfo]:
    neds: List[NedInfo] = []
    for ned in root.devices.ned_ids.ned_id:
        ni = NedInfo.from_cdb(ned.id)
        neds.append(ni)
    return neds


def get_neds_by_family(loaded_neds: List[NedInfo]) -> Dict[str, List[NedInfo]]:
    fs = {}
    for family, neds in groupby(loaded_neds, lambda n: str(n.family)):
        fs[family] = sorted(neds)
    return fs


def find_ned_by_family(loaded_neds: List[NedInfo], family: Identity) -> Optional[NedInfo]:
    families = get_neds_by_family(loaded_neds)
    if str(family) in families:
        neds = reversed(sorted(families[str(family)]))
        return list(neds)[0]
    else:
        return None
