import ncs

try:
    from ncs.cdb import Subscriber  # type: ignore
except ImportError:
    from ncs.experimental import Subscriber # type: ignore

from alarm_sink import alarm_sink

from . import device_alarm
from .common import classes, utils


class ClearMepAlarmSubscriberIter(classes.SubscriberIterSkipSlave):
    """Callbacks for CDB subscriber for clearing (device automaton) management endpoint alarms

    The subscriber will clear any outstanding alarms for deleted management
    endpoints for a device automaton service instance. To avoid duplicate work
    / race condition the subscriber will only clear alarms when the device
    automaton instance remains active. In other words, if the automaton service
    instance is removed, all device-related alarms are cleared in the device
    delete function instead. This is accomplished by subscribing only to the
    list of management endpoints and *not* using the ITER_WANT_ANCESTOR_DELETE
    flag.
    """
    def __init__(self, log):
        self.log = classes.LoggerWrapper(log, 'device-automaton-clear-mep-alarm')

    def register(self, subscriber):
        subscriber.register('/ncs:devices/devaut:automaton/management-endpoint', priority=101, iter_flags=0, iter_obj=self)

    def pre_iterate(self):
        return []

    def iterate(self, keypath, operation, oldval, newval, state):
        # state is pre-initialized to []
        self.log.debug(f'iterate: {keypath} {operation} {oldval} {newval}')
        if operation == ncs.MOP_DELETED and len(keypath) == 5:
            # The HKeypathRef to a management-endpoint list entry is a list
            # with 5 entries (with components in reverse order)
            mep = {'device': str(keypath[2][0]), 'mep': str(keypath[0][0])}
            state.append(mep)

        # look at siblings
        return ncs.ITER_CONTINUE

    def post_iterate(self, state):
        self.log.debug('post_iterate, state=', state)
        with utils.maapi_session('python-devaut-clear-mep-alarm', 'system') as m:
            ask = alarm_sink.AlarmSink(m)
            for mep in state:
                device_alarm.clear_device_automaton_alarm(ask, mep['device'], mep['mep'])


class ClearMepAlarmSubscriber(Subscriber):
    def start(self):
        iterator = ClearMepAlarmSubscriberIter(log=self.log)
        iterator.register(self)
        super().start()
