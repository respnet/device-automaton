import random
import base64
import crypt
import scrypt
from passlib.hash import bcrypt, md5_crypt, sha512_crypt
from passlib.crypto.digest import pbkdf2_hmac

import ncs
from ncs.application import Service
from ncs.experimental import Query
from . import TemplateName
from .common import utils, classes


def cisco_scrypt(pwd: str) -> str:
    """Cisco flavor of scrypt

    Code obtained from https://github.com/wifiwizardofoz/ciscoPWDhasher"""
    std_b64chars  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    cisco_b64chars = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    b64table = str.maketrans(std_b64chars, cisco_b64chars)

    # Create random salt (Cisco use 14 characters from custom B64 table)
    salt_chars=[]
    for _ in range(14):
        salt_chars.append(random.choice(cisco_b64chars))
    salt = "".join(salt_chars)
    # Create the hash
    digest = scrypt.hash(pwd.encode(), salt.encode(), 16384, 1, 1, 32)
    # Convert the hash from Standard Base64 to Cisco Base64
    digest = base64.b64encode(digest).decode().translate(b64table)[:-1]
    return f'$9${salt}${digest}'


def cisco_pbkdf2(pwd: str) -> str:
    """Cisco flavor of pbkdf2

    This is using pbkdf2 with 5000 rounds and then the digest is truncated to 16
    bytes. Code adapted from
    https://foss.heptapod.net/python-libs/passlib/-/issues/87#note_159147"""
    rounds = 5000
    # Truncate the '$6$' prefix from the salt
    salt = crypt.mksalt(crypt.METHOD_SHA512)[3:].encode('utf-8')
    # Take the first 16 bytes of the hash with 5000 rounds
    digest = pbkdf2_hmac('sha512', salt, pwd, rounds)[0:16]
    return '$sha512${rounds}${salt}${digest}'.format(
        rounds=rounds,
        salt=base64.b64encode(salt).decode('utf-8'),
        digest=base64.b64encode(digest).decode('utf-8')
    )


def _find_user_ordering_index(user_list: ncs.maagic.List, username: str) -> int:
    """Find existing user ordering-index or select a new one

    If the user entry already exists, we must use the same ordering-index. If it
    does not exist, select the next value by adding 1 to the highest value."""
    # I tried using ncs.maagic.List.filter, but it did not work from Service.pre_modification?!
    with Query(ncs.maagic.get_trans(user_list), f"{user_list._name}[name='{username}']",
               context_node=user_list._parent._path,
               select=['ordering-index'], result_as=ncs.QUERY_STRING) as q:
        try:
            return int(next(q)[0])
        except StopIteration:
            existing_entries = [k[0].as_pyval() for k in user_list.keys()]
            return max(existing_entries, default=-1) + 1


class UserAccount(ncs.application.Service):
    @Service.pre_modification
    def cb_pre_modification(self, tctx, op, kp, root, proplist):
        if op in (ncs.dp.NCS_SERVICE_CREATE, ncs.dp.NCS_SERVICE_UPDATE):
            device = root.ncs__devices.device[kp[2]]
            username = str(kp[0][0])
            dc = classes.DeviceCapability(device)
            if dc.check_capability('http://cisco.com/ns/yang/Cisco-IOS-XR-aaa-locald-cfg') \
               and dc.check_capability('http://cisco.com/ns/yang/Cisco-IOS-XR-aaa-lib-cfg'):
                index = _find_user_ordering_index(device.config.aaa_lib_cfg__aaa.usernames.username, username)
            elif dc.check_capability('http://cisco.com/ns/yang/Cisco-IOS-XR-um-aaa-cfg'):
                index = _find_user_ordering_index(device.config.um_aaa_cfg__aaa.usernames.username, username)
            else:
                index = 0
            self.log.debug(f'{kp}: pre_modification: selected ordering-index {index} for {username}')
            pd = utils.pl2pd(proplist)
            pd["index"] = str(index)
            proplist = utils.pd2pl(pd)
        return proplist

    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        propdict = utils.pl2pd(proplist)
        service.index = propdict["index"]
        self.log.info('{}: cb_create'.format(service._path))
        tv = ncs.template.Variables()
        template = ncs.template.Template(service)

        encrypted_password = service.password
        # Some devices still need a password set on a user account, even though
        # they're supposed to use SSH key auth. This is a workaround to generate
        # a persistent random password
        if encrypted_password is None:
            if "password-fake-random" not in propdict:
                fake_random_password = utils.random_string(21)
                propdict["password-fake-random"] = fake_random_password
                propdict["password-cleartext"] = ""
                propdict["password-md5-4"] = ""
                propdict["password-md5-8"] = ""
                propdict["password-cisco-scrypt"] = ""
                propdict["password-cisco-pbkdf2"] = ""
                propdict["password-sha512"] = ""
                #ALU
                propdict["password-bcrypt-2y"] = bcrypt.using(rounds=10, ident="2y").hash(fake_random_password)
                # $1c$e]#BE4DQ[*$pghx*dqa]'W/@$;|qvg)dcyF!P\5D,l3KU#)C6E0$
                # Hardcoded password because we don't know algorithm for encrypting password
                # and want to keep device in sync.
                # This was generated by Kristian Larsson in such a way that the input clear text password was immediately discarded.
                propdict["password-huawei"] = "$1c$e]#BE4DQ[*$pghx*dqa]'W/@$;|qvg)dcyF!P\\5D,l3KU#)C6E0$"
            self.log.info("Using fake random password")
        else:
            password = utils.decrypt_encrypted_string(root, encrypted_password)
            # Store cleartext password in propdict so we know if it has changed
            # between runs. Store hashed passwords in the propdict object to avoid
            # regenerating for every run, since that would generate new password
            # due to salting, which in turn would lead to updating the config on
            # all boxes all the time

            # MD5 hash with 4 character salt for XR devices
            if "password-md5-4" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-md5-4"] = md5_crypt.using(salt_size=4).hash(password)
            # MD5 hash with 8 character salt for JUNOS devices
            if "password-md5-8" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-md5-8"] = md5_crypt.using(salt_size=8).hash(password)
            # bcrypt for ALU devices
            if "password-bcrypt-2y" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-bcrypt-2y"] = bcrypt.using(rounds=10, ident="2y").hash(password)
            # scrypt for Cisco (IOS) devices
            if "password-cisco-scrypt" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-cisco-scrypt"] = cisco_scrypt(password)
            if "password-cisco-pbkdf2" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-cisco-pbkdf2"] = cisco_pbkdf2(password)
            if "password-sha512" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-sha512"] = sha512_crypt.using(rounds=5000).hash(password)
            # HUAWEI
            if "password-huawei" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-huawei"] = password
            # cleartext
            if "password-cleartext" not in propdict or propdict["password-cleartext"] != password:
                propdict["password-cleartext"] = password

            if "password-fake-random" in propdict:
                del propdict['password-fake-random']

        tv.add("PASSWORD_HUAWEI", propdict["password-huawei"])  # HUAWEI password
        tv.add("PASSWORD_BCRYPT", propdict["password-bcrypt-2y"])  # ALU
        tv.add("PASSWORD_MD5_4", propdict["password-md5-4"])  # XR
        tv.add("PASSWORD_MD5_8", propdict["password-md5-8"])  # JUNOS
        tv.add("PASSWORD_CISCO_SCRYPT", propdict["password-cisco-scrypt"]) # Cisco IOS
        tv.add("PASSWORD_CISCO_PBKDF2", propdict["password-cisco-pbkdf2"]) # Cisco ASA
        tv.add("PASSWORD_SHA512", propdict["password-sha512"]) # Cisco IOS XR 7.x+
        tv.add("PASSWORD", utils.decrypt_encrypted_string(root, encrypted_password)) # IOS
        self.log.info("service password: ", service.password)
        # On Huawei, we need to create a user with a "fake random" password
        # regardless of only wanting to use key-based auth. To enable/disable
        # password auth, we set the VRP_AUTHENTICATION_TYPE variable
        auth_type = ""
        rsa_key = False
        for ak in service.authorized_key:
            if ak.algorithm == 'ssh-rsa':
                rsa_key = True
        if service.password is not None and rsa_key:
            auth_type = "password-rsa"
        elif service.password is not None:
            auth_type = "password"
        elif rsa_key:
            auth_type = "rsa"
        tv.add("VRP_AUTHENTICATION_TYPE", auth_type)

        template.apply(TemplateName('devaut-user-account'), tv)

        return utils.pd2pl(propdict)


class ServiceApp(ncs.application.Application):
    def setup(self):
        self.register_service('devaut-user-account-servicepoint', UserAccount)
