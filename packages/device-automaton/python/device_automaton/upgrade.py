import ncs
import _ncs


def _upgrade_authgroup(cdbsock, trans) -> bool:
    print("Starting /devices/authgroups/devaut:authgroup upgrade")
    num = _ncs.cdb.num_instances(cdbsock, "/ncs:devices/authgroups/devaut:authgroup")

    try:
        for i in range(0, num):
            name = _ncs.cdb.get(cdbsock, f"/ncs:devices/authgroups/devaut:authgroup[{i}]/name")
            kp = f"/ncs:devices/authgroups/devaut:authgroup{{{name}}}"
            try:
                rsa_private_key = _ncs.cdb.get(cdbsock, f"{kp}/rsa-private-key")
            except _ncs.error.Error as e:
                if "Bad path element" in str(e):
                    print(f"{kp}/rsa-private-key does not exist, migration completed")
                    return True
                elif "item does not exist" in str(e):
                    rsa_private_key = None
                else:
                    raise

            if rsa_private_key is None:
                print(f"{kp}/rsa-private-key is not set")
                continue

            print(f"Set {kp}/private-key to '***'")
            trans.set_elem(rsa_private_key, f"{kp}/private-key")

    except _ncs.error.Error as e:
        print(f"Failed to migrate /ncs:devices/authgroups/devaut:authgroup {e}")
        return False
    print("Completed /ncs:devices/authgroups/devaut:authgroup upgrade")
    return True


def _upgrade_user_account(cdbsock, trans) -> bool:
    print("Starting /ncs:devices/device/devaut:user-account upgrade")
    dev_num = _ncs.cdb.num_instances(cdbsock, "/ncs:devices/device")

    try:
        for i in range(0, dev_num):
            dev_name = _ncs.cdb.get(cdbsock, f"/ncs:devices/device[{i}]/name")
            dev_kp = f"/ncs:devices/device{{{dev_name}}}"
            num = _ncs.cdb.num_instances(cdbsock, f"{dev_kp}/devaut:user-account")
            for j in range(0, num):
                name = _ncs.cdb.get(cdbsock, f"{dev_kp}/devaut:user-account[{j}]/username")
                kp = f"{dev_kp}/devaut:user-account{{{name}}}"
                try:
                    rsa_public_key = _ncs.cdb.get(cdbsock, f"{kp}/rsa-key")
                except _ncs.error.Error as e:
                    if "Bad path element" in str(e):
                        print(f"{kp}/rsa-key does not exist, migration completed")
                        return True
                    elif "item does not exist" in str(e):
                        rsa_public_key = None
                    else:
                        raise

                if rsa_public_key is None:
                    print(f"{kp}/rsa-key is not set")
                    continue

                print(f"Create {kp}/authorized-key{{{name}}} for RSA")
                trans.safe_create(f"{kp}/keypair{{{name}}}")
                trans.set_elem(rsa_public_key, f"{kp}/keypair{{{name}}}/key-data")
                trans.set_elem("ssh-rsa", f"{kp}/keypair{{{name}}}/algorithm")

    except _ncs.error.Error as e:
        print(f"Failed to migrate /ncs:devices/device/devaut:user-account {e}")
        return False
    print("Completed /ncs:devices/device/devaut:user-account upgrade")
    return True


def _upgrade_automaton_initial_credentials(cdbsock, trans) -> bool:
    print("Starting /ncs:devices/devaut:automaton/management-endpoint/initial-credentials upgrade")
    dev_num = _ncs.cdb.num_instances(cdbsock, "/ncs:devices/devaut:automaton")

    try:
        for i in range(0, dev_num):
            dev_name = _ncs.cdb.get(cdbsock, f"/ncs:devices/devaut:automaton[{i}]/device")
            dev_kp = f"/ncs:devices/devaut:automaton{{{dev_name}}}"
            mep_num = _ncs.cdb.num_instances(cdbsock, f"{dev_kp}/management-endpoint")
            for j in range(0, mep_num):
                mep_name = _ncs.cdb.get(cdbsock, f"{dev_kp}/management-endpoint[{j}]/address")
                mep_kp = f"{dev_kp}/management-endpoint{{{mep_name}}}"
                num = _ncs.cdb.num_instances(cdbsock, f"{mep_kp}/initial-credentials")
                for k in range(0, num):
                    name = _ncs.cdb.get(cdbsock, f"{mep_kp}/initial-credentials[{k}]/username")
                    kp = f"{mep_kp}/initial-credentials{{{name}}}"
                    try:
                        rsa_private_key = _ncs.cdb.get(cdbsock, f"{kp}/rsa-private-key")
                    except _ncs.error.Error as e:
                        if "Bad path element" in str(e):
                            print(f"{kp}/rsa-private-key does not exist, migration completed")
                            return True
                        elif "item does not exist" in str(e):
                            rsa_private_key = None
                        else:
                            raise

                    if rsa_private_key is None:
                        print(f"{kp}/rsa-private-key is not set")
                        continue

                    rsa_public_key = _ncs.cdb.get(cdbsock, f"{kp}/rsa-public-key")

                    print(f"Create {kp}/keypair{{{rsa_public_key}}} for RSA")
                    trans.safe_create(f"{kp}/keypair{{{rsa_public_key}}}")
                    trans.set_elem(rsa_private_key, f"{kp}/keypair{{{rsa_public_key}}}/private-key")
                    trans.set_elem("ssh-rsa", f"{kp}/keypair{{{rsa_public_key}}}/algorithm")

    except _ncs.error.Error as e:
        print(f"Failed to migrate /ncs:devices/devaut:automaton/management-endpoint/initial-credentials {e}")
        return False
    print("Completed /ncs:devices/devaut:automaton/management-endpoint/initial-credentials upgrade")
    return True


def _upgrade_automaton_management_credentials(cdbsock, trans) -> bool:
    """Migrate SSH key in /ncs:devices/devaut:automaton/management-credentials

    Create the `keypair` list entry with the data from the `rsa-private-key` and
    `rsa-public-key` leafs. Use this code in your (node) services to migrate the
    data elsewhere where you use the `device-automaton-grouping` grouping."""

    print("Starting /ncs:devices/devaut:automaton/management-credentials upgrade")
    num = _ncs.cdb.num_instances(cdbsock, "/ncs:devices/devaut:automaton")

    try:
        for i in range(0, num):
            name = _ncs.cdb.get(cdbsock, f"/ncs:devices/devaut:automaton[{i}]/device")
            kp = f"/ncs:devices/devaut:automaton{{{name}}}/management-credentials"
            try:
                rsa_private_key = _ncs.cdb.get(cdbsock, f"{kp}/rsa-private-key")
            except _ncs.error.Error as e:
                if "Bad path element" in str(e):
                    print(f"{kp}/rsa-private-key does not exist, migration completed")
                    return True
                elif "item does not exist" in str(e):
                    rsa_private_key = None
                else:
                    raise

            if rsa_private_key is None:
                print(f"{kp}/rsa-private-key is not set")
                continue

            rsa_public_key = _ncs.cdb.get(cdbsock, f"{kp}/rsa-public-key")

            print(f"Create {kp}/keypair{{{rsa_public_key}}} for RSA")
            trans.safe_create(f"{kp}/keypair{{{rsa_public_key}}}")
            trans.set_elem(rsa_private_key, f"{kp}/keypair{{{rsa_public_key}}}/private-key")
            trans.set_elem("ssh-rsa", f"{kp}/keypair{{{rsa_public_key}}}/algorithm")

    except _ncs.error.Error as e:
        print(f"Failed to migrate /ncs:devices/devaut:automaton/management-credentials {e}")
        return False
    print("Completed /ncs:devices/devaut:automaton/management-credentials upgrade")
    return True


class Upgrade(ncs.upgrade.Upgrade):
    def upgrade(self, cdbsock, trans):
        print("Starting automaton upgrade")
        # start a session against running
        _ncs.cdb.start_session2(cdbsock, ncs.cdb.RUNNING, ncs.cdb.LOCK_SESSION | ncs.cdb.LOCK_WAIT)

        if not _upgrade_authgroup(cdbsock, trans):
            exit(1)
        if not _upgrade_user_account(cdbsock, trans):
            exit(1)
        if not _upgrade_automaton_initial_credentials(cdbsock, trans):
            exit(1)
        if not _upgrade_automaton_management_credentials(cdbsock, trans):
            exit(1)
        print("Completed automaton upgrade")
