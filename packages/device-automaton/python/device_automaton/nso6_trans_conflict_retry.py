"""Backwards compatible wrappers for NSO 6.0 optimistic concurrency conflict resolution

NSO 6.0 runs (some) transactions in parallel using the new optimistic
concurrency approach. Some of these transactions may end conflicting with
others. The 6.0 Python API includes two methods for retrying a transaction that
failed to commit because of a conflict:
1. the @retry_on_conflict() decorator, to be used on action callpoints or other
generic functions. The decorator will just retry the wrapped function,
2. the ncs.maaapi.Maapi.run_with_retry(fun) method that will start a write
transaction and retry failed apply calls.

This module provides "no-op" shims for the new functions.
"""
import functools
import ncs

if callable(getattr(ncs.maapi, 'retry_on_conflict', None)):
    retry_on_conflict = ncs.maapi.retry_on_conflict
else:
    def retry_on_conflict(retries=10, log=None):
        def decorate(fn):
            @functools.wraps(fn)
            def wrapper(*args, **kwargs):
                return fn(*args, **kwargs)
            return wrapper
        return decorate

    def __maapi_run_with_retry(self, fun, max_num_retries=10, commit_params: ncs.maapi.CommitParams=None, usid=0, flags=0, vendor=None, product=None, version=None, client_id=None) -> bool:
       with self.start_write_trans(usid=usid, flags=flags, vendor=vendor, product=product, version=version, client_id=client_id) as trans:
           if fun(trans):
               trans.apply_params(keep_open=False, params=commit_params)
               return True
           else:
               return False
    ncs.maapi.Maapi.run_with_retry = __maapi_run_with_retry
