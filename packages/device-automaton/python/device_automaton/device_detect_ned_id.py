import collections
import re
from dataclasses import dataclass
from decimal import Decimal
from typing import Dict, Optional, Set, Tuple

import ncs

from .ned_id import Identity


@dataclass(frozen=True)
class YangModule:
    """Store information that uniquely identifies a YANG model

    Depending on how the information about the YANG model was collected
    (capabilities exchange), we may know only the namespace of the models.
    """

    namespace: str
    name: Optional[str] = None
    revision: Optional[str] = None


# Type alias for the dictionary storing mappings between device-id and the set
# of YANG modules
CapabilitiesMapping = Dict[str, Set[YangModule]]
DeviceNedIntersect = Dict[str, Dict[str, Decimal]]


def extract_data(root: ncs.maagic.Root, device: Optional[str] = None, ned_filter: Optional[Identity] = None) \
        -> Tuple[CapabilitiesMapping, CapabilitiesMapping]:
    """Extract modules (provided by NEDs) and device capabilities from CDB

    This function outputs two dictionaries containing YANG module information
    stored in a YangModule DTO.

    :param root: maagic Root node
    :param device: optional device name, defaults to all devices
    :param ned_filter: optional Identity object used to limit the NED search space using a base identity
    :return: two dictionaries (devices, neds) with YANG module information
    """
    neds: CapabilitiesMapping = {}
    devices: CapabilitiesMapping = {}

    for ned in root.devices.ned_ids.ned_id:
        if ned_filter:
            ned_id = Identity.from_cdb(ned.id)
            if ned_id.derived_from(ned_filter, or_self=True) is False:
                continue
        neds[ned.id] = set()
        for module in ned.module:
            module_info = YangModule(module.namespace, module.name, module.revision)
            neds[ned.id].add(module_info)

    if device is None:
        devices_iter = iter(root.devices.device)
    else:
        devices_iter = iter([root.devices.device[device]])
    for d in devices_iter:
        devices[d.name] = set()
        for capability in d.capability:
            capability_info = YangModule(capability.uri, capability.module, capability.revision)
            devices[d.name].add(capability_info)

    return devices, neds


def _weighted_score(capabilities: Set[YangModule]) -> Decimal:
    """Calculate the weighted score for matching YANG models

    When counting the matches, some standard models should have a lower
    score. This is done to prevent NEDs with a lot of standard modules
    included from overshadowing legit NEDs that contain actual device
    models.
    """

    weights = {r'^urn:ietf:params:netconf:capability:.*|^:.*': Decimal(0),
               r'^urn:ietf:params:xml:ns:netconf.*': Decimal(0),
               r'^urn:ietf:params:xml:ns:yang:.*': Decimal('0.1'),
               r'^http://tail-f.com/ns/.*|^http://tail-f.com/yang/.*': Decimal('0.1')}

    score = Decimal(0)
    for c in capabilities:
        for pattern, weight in weights.items():
            if re.match(pattern, c.namespace):
                score += weight
                break
        else:
            # the loop wasn't break-en, this is an exact non-weighted match
            score += 1
    return score


def score_devices_to_ned_ids(devices: CapabilitiesMapping, neds: CapabilitiesMapping) -> DeviceNedIntersect:
    """Calculate the scores of matching the given devices amongst the supported NEDs

    The function finds intersects between the set of YANG modules provided by
    the NEDs and device capabilities. The score is normalized on the number of
    device capabilities.

    :param devices: dict of devices with their capabilities
    :param neds: dict of ned-ids with their provided modules
    :return: dict of matched devices with ned-ids
    """

    # Match the device capabilities with YANG models provided by NEDs. The
    # device_caps_ned_intersect dictionary will contain the cumulative match
    # score of individual NED modules with device capabilities.
    # The dictionary is keyed on device and contains a nested dictionary of
    # (ned-id, score) key-value pairs for each device. We use defaultdict to
    # initialize the nested dictionary automatically.
    device_caps_ned_intersect: DeviceNedIntersect = collections.defaultdict(dict)

    for ned, modules in neds.items():
        for device, capabilities in devices.items():
            # The modules and capabilities loop variables are sets of
            # YangModule objects (namespace, name, revision tuple). Set
            # operators intersect (&) and substract (-) are used in filtering.
            # The base score is determined by intersecting the two sets to find
            # the number of exact matches.
            if len(modules) == 0 or len(capabilities) == 0:
                device_caps_ned_intersect[device][ned] = Decimal(0)
                continue
            exact_matches = modules & capabilities
            device_caps_ned_intersect[device][ned] = _weighted_score(exact_matches) / _weighted_score(capabilities) * Decimal('0.75')

            # Bump the score by including YANG models where only the namespace
            # matches. This is necessary for cases where the advertised device
            # capabilities do not include module names or revisions.
            # The candidates for this match are all modules except the exact
            # matches counted in the previous step.
            fuzzy_candidates = modules - exact_matches
            fuzzy_ns = set(x.namespace for x in fuzzy_candidates)
            device_ns = set(x.namespace for x in capabilities)
            fuzzy_matches = {YangModule(ns) for ns in fuzzy_ns & device_ns}

            # Is 0.1 a good factor? It is enough to tip the scales in favor of
            # the likely correct NED when only namespaces are known, but not
            # enough so as to have an effect where there are exact matches.
            device_caps_ned_intersect[device][ned] += _weighted_score(fuzzy_matches) * Decimal('0.25') / _weighted_score(capabilities)
    return device_caps_ned_intersect


def find_best_ned_id(device_caps_ned_intersect: DeviceNedIntersect) -> Dict[str, Optional[str]]:
    """Find the ned-id with the highest score for the given devices and score

    The device_caps_ned_intersect dict contains the match score of individual
    NED modules with device capabilities. To find the best fit for a device, we
    find the NED with the best score.
    """
    result: Dict[str, Optional[str]] = {}
    for d, n in device_caps_ned_intersect.items():
        largest_intersect = max(n, key=lambda x: n[x])
        if n[largest_intersect] > 0:
            result[d] = largest_intersect
        else:
            result[d] = None
    return result


def match_devices_to_ned_ids(root: ncs.maagic.Root, device: Optional[str] = None, ned_filter: Optional[Identity] = None) -> DeviceNedIntersect:
    """Helper function for calculating the scores for ned-id fit for devices"""
    return score_devices_to_ned_ids(*extract_data(root, device, ned_filter))


if __name__ == '__main__':
    with ncs.maapi.single_read_trans('python-devaut-detect-ned-id', 'system',
                                     db=ncs.OPERATIONAL) as t:
        _root = ncs.maagic.get_root(t)
        print(match_devices_to_ned_ids(_root))
