import datetime
import logging
import time
import traceback
from typing import Optional, Tuple

import ncs

from . import enqueue_work, futurama, service_log
from .common import utils


def retry_device_automatons():
    log = logging.getLogger('device-automaton-retrier')
    # Exceptions unhandled by the retry_device_automatons function will cause the worker process to
    # die. The background worker function wrapper will log the error and the supervisor will restart
    # the worker automatically.
    with utils.maapi_session('python-devaut-service-retrier', 'system') as maapi:
        loop_function(maapi, log)


def loop_function(maapi: ncs.maapi.Maapi, log: logging.Logger):
    while True:
        time.sleep(30)
        retriable_services = find_retriable_services(maapi, log)
        if len(retriable_services) == 0:
            log.debug('no failed device automatons found')
            continue

        futurama.concurrent_worker(retriable_services,
                                   retry_service,
                                   log=log,
                                   auto_retry=False)


def retry_service(service_kp: str) -> Tuple[bool, Optional[str]]:
    log = logging.getLogger('device-automaton-retrier')
    with utils.maapi_session('python-devaut-service-retrier-{}/enqueue-work'.format(service_kp), 'system') as maapi:
        log.info('{}: retrying device automaton with action enqueue-work'.format(service_kp))
        retry_start = datetime.datetime.utcnow()
        with maapi.start_read_trans(db=ncs.OPERATIONAL) as oper_t_read:
            service = ncs.maagic.get_node(oper_t_read, service_kp)
            try:
                result = enqueue_work.action(service, enqueue_work.InternalSource.RETRIER)
                success = result.success
                message = result.message
            except Exception as e:
                success = False
                message = 'error retrying action enqueue-work: {}: {}'.format(type(e).__name__, e)
                log.error('{}: error retrying action enqueue-work: {}'.format(service_kp, traceback.format_exc()))

        # log the retry attempt to the service being retried's /log container
        if not success:
            with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                write_service = ncs.maagic.get_node(oper_t_write, service)
                service_log.log(write_service,
                                entry_type='devaut:service-retried',
                                level='error',
                                message=message,
                                when=utils.format_yang_date_and_time(retry_start))
                oper_t_write.apply()

        return success, message


def find_retriable_services(maapi: ncs.maapi.Maapi, log: logging.Logger):
    """Finds failed device automaton instances.

    Failed instances have the `$service/plan/failed` leaf set.

    :return: list of device automaton instances
    """
    retriable_services = []

    with maapi.start_read_trans(db=ncs.OPERATIONAL) as oper_t_read:
        def cb(kp, value_unused):
            service_kp = str(kp[2:])
            service = ncs.maagic.get_node(oper_t_read, service_kp)

            # check if service retrier was disabled for this service
            disabled_until = utils.convert_yang_date_and_time_to_datetime(service.service_retrier.disabled_until)
            if disabled_until is not None and disabled_until > datetime.datetime.utcnow():
                log.info('{}: service retrier disabled until {}'.format(service._path, disabled_until))
                return

            retriable_services.append(service_kp)

        oper_t_read.xpath_eval('/ncs:devices/devaut:automaton/plan/failed', cb, trace=None, path='')

    return retriable_services
