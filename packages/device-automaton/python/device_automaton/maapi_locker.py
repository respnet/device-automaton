import logging
import time
import contextlib
from typing import Optional, Tuple, Iterator

import ncs

class MaapiLocker:
    """MaapiLocker is a convenience wrapper around MAAPI partial lock

    NSO allows applications to acquire a global or partial CDB lock. The MAAPI
    function `lock_partial` is non-blocking and will return various error
    messages. This class simplifies locking with the use of a context manager.

        with MaapiLocker(maapi).lock("/devices/device[name='foo']", timeout=60) as (success, error):
            if not success:
                log.error(error)
            else:
                # do something

    """
    def __init__(self, maapi: ncs.maapi.Maapi, log: Optional[logging.Logger] = None):
        self.maapi = maapi
        self.log = log or logging.getLogger('maapi-locker')

    @contextlib.contextmanager
    def lock(self, path: str, timeout: Optional[int] = None) -> Iterator[Tuple[bool, str]]:
        """Context-managed partial MAAPI lock

        :param path: string XPath for the partial lock
        :param timeout: optional timeout in seconds, will block
        :returns: tuple of (success, error_message)

        If the timeout parameter is not provided, the method will block until
        the lock is acquired.
        """
        lock_id = None
        start_time = time.time()
        while lock_id is None:
            if timeout and time.time() - start_time > timeout:
                self.log.error(f'Timed out after {timeout} waiting for partial lock on {path}')
                yield False, 'timeout'
                return

            try:
                lock_id = self.maapi.lock_partial(ncs.RUNNING, [path])
            except ncs.error.Error as e:
                if e.confd_errno in (ncs.ERR_LOCKED, ncs.ERR_NOEXISTS):
                    if e.confd_errno == ncs.ERR_LOCKED:
                        reason = 'it is locked'
                    else:
                        reason = 'it does not exist'
                    self.log.info(f'Partial lock on {path} failed because {reason}, retrying after 1s')
                    time.sleep(1)
                else:
                    self.log.error(f'Partial lock on {path} failed with exception: {e}')
                    yield False, str(e)
                    return

        # try..finally to make sure the lock is released even if an exception occurs
        # in the caller
        try:
            yield True, None
        finally:
            self.maapi.unlock_partial(lock_id)
