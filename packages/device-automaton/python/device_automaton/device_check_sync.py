import dataclasses
from datetime import datetime
from typing import Dict, Optional
from alarm_sink import alarm_sink

import _ncs
import ncs

from . import futurama, enqueue_work
from .common import utils


def _is_authentication_error(sync_result: str, info: Optional[str]) -> bool:
    # error message can be: 'Failed to authenticate', 'authentication failed',
    # thus matching on 'authenticat' will catch 'em all.
    return sync_result == 'error' and info is not None and 'authenticat' in info


@dataclasses.dataclass
class DeviceCheckSyncResult:
    sync_result: Optional[str] = None
    info: Optional[str] = None
    end_timestamp: Optional[datetime] = None
    start_timestamp = datetime.utcnow()
    # Total duration of the check-sync operation in hundredths of a second
    duration: Optional[int] = None

    def update_from_check_sync_result(self, sync_result: str, info: Optional[str]):
        self.sync_result = sync_result
        if info:
            self.info = info.strip()
        else:
            self.info = None
        self.end_timestamp = datetime.utcnow()
        self.duration = int((self.end_timestamp - self.start_timestamp).total_seconds() * 100)


class DeviceCheckSync:
    def __init__(self, maapi, log):
        self.log = log
        self.maapi = maapi

    @staticmethod
    def _calc_check_sync_timeout(number_of_devices, read_timeout, window_size, buffer):
        """We need to check how much time we are gonna spend for check-sync (worst case) and set the
        timeout for action accordingly -> (num_of_devices / window_size) * read_timeout + buffer. If the
        value is smaller than number of devices we are checking worst case is read timeout for a device.
        """
        action_timeout = (int(number_of_devices / window_size) * read_timeout) + buffer
        if action_timeout < read_timeout:
            action_timeout = read_timeout + buffer
        return action_timeout

    def _compare_config(self, device_name: str) -> dict:
        """Execute an expensive compare-config on given device.

        If there is a diff (out-of-sync), raise out-of-sync major alarm."""
        with ncs.maapi.single_read_trans('python-devaut-compare-config', 'system') as t_read:
            root = ncs.maagic.get_root(t_read)
            device = root.devices.device[device_name]
            device_xpath = f"/ncs:devices/ncs:device[ncs:name='{device_name}']"
            try:
                compare_result = device.compare_config()
            except Exception as e:
                self.log.error(f'Failed compare-config on device {device_name}, error {e}')
                return {'sync-result': 'error', 'info': f'error {e}'}
            if compare_result.info:
                return {'sync-result': 'error', 'info': compare_result.info}
            elif compare_result.diff:
                self.log.debug('{}: compare-config debug result: {}'.format(device._path, compare_result.diff))
                # like the built-in check-sync action, raise out-of-sync alarm
                with alarm_sink.AlarmSink() as ask:
                    alarm_id = alarm_sink.AlarmId(device_name, device_xpath, 'out-of-sync', None)
                    alarm = alarm_sink.Alarm(alarm_id, severity=alarm_sink.PerceivedSeverity.MAJOR,
                            alarm_text='Out of sync due to no-networking or failed commit-queue commits (compare-config)')
                    ask.submit_alarm(alarm)
                return {'sync-result': 'out-of-sync', 'info': None}
            # like the built-in check-sync action, clear the out-of-sync alarm
            with alarm_sink.AlarmSink() as ask:
                alarm_id = alarm_sink.AlarmId(device_name, device_xpath, 'out-of-sync', None)
                alarm = alarm_sink.Alarm(alarm_id, severity=alarm_sink.PerceivedSeverity.MAJOR,
                                         alarm_text=None)
                alarm.cleared = True
                ask.submit_alarm(alarm)

            return {'sync-result': 'in-sync', 'info': None}

    def device_check_sync(self, devices, uinfo=None) -> Dict[str, DeviceCheckSyncResult]:
        """Execute "cheap" check-sync on device, with a fallback to compare-config

        Some NETCONF devices do not support check-sync. In that case, we can fallback to the more
        expensive compare-config. The method will return a tuple of (result, info), where result is
        an enumeration defined in check-sync-result grouping.
        """
        unchecked_devices = set(devices)
        results = {device: DeviceCheckSyncResult() for device in unchecked_devices}
        retries = 1

        # device read timeout
        with self.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            read_timeout = root.devices.global_settings.read_timeout
            self.log.debug('DeviceCheckSync: Read timeout for devices set to {}'.format(read_timeout))

            max_retries = 5
            while unchecked_devices and retries <= max_retries:
                self.log.debug('DeviceCheckSync: {}/{}: need to check {} devices: {}'
                               .format(retries, max_retries, len(unchecked_devices), unchecked_devices))
                if uinfo:
                    # set timeout to avoid action timeout
                    action_timeout = self._calc_check_sync_timeout(len(unchecked_devices), read_timeout, 100, 10)
                    _ncs.dp.action_set_timeout(uinfo, action_timeout)
                    self.log.debug('DeviceCheckSync: Action timeout set to {}, {} unchecked devices left'.format(action_timeout, len(unchecked_devices)))

                unsupported = []
                # execute cheap check-sync
                action_input = root.devices.check_sync.get_input()
                action_input.device = unchecked_devices
                result = root.devices.check_sync(action_input)
                for device_result in result.sync_result:
                    self.log.debug('DeviceCheckSync: Device {} check-sync result: {} ({})'
                            .format(device_result.device, device_result.result, device_result.info))
                    if device_result.result == 'unsupported':
                        unsupported.append(device_result.device)
                    else:
                        results[device_result.device].update_from_check_sync_result(device_result.result, device_result.info)
                        if device_result.result != 'error' or _is_authentication_error(device_result.result, device_result.info):
                            # either we got a definitive result (in-sync or
                            # out-of-sync), or there was an authentication-related error
                            unchecked_devices.remove(device_result.device)

                    if uinfo:
                        # reset action timeout based on number of workers and unsupported devices we need to check-sync
                        action_timeout = self._calc_check_sync_timeout(len(unsupported), read_timeout, 20, 10)
                        _ncs.dp.action_set_timeout(uinfo, action_timeout)

                    # do expensive check-sync for unsupported devices
                    if unsupported:
                        compare_results = futurama.concurrent_worker(unsupported, self._compare_config, max_workers=20, auto_retry=False)
                        for device, result in compare_results.items():
                            self.log.debug('DeviceCheckSync: Device {} compare-config result: {} (info: {})'
                                           .format(device, result['sync-result'], result['info']))
                            results[device].update_from_check_sync_result(result['sync-result'], result['info'])
                            if result['sync-result'] != 'error' or _is_authentication_error(result['sync-result'], result['info']):
                                # either we got a definitive result (in-sync or
                                # out-of-sync), or there was an authentication-related error
                                unchecked_devices.remove(device)

                retries += 1


        # iterate through results where sync_result is not set - some error
        # prevented check-sync from completing (maybe retries exceeded?). set
        # the sync_result to 'error'. this ensures that all device results
        # returned from this function have the attributes sync_result and
        # end_timestamp set
        for device, result in results.items():
            if not result.sync_result:
                result.update_from_check_sync_result('error', 'Missing check-sync result')
                self.log.error(f'DeviceCheckSync: Missing check-sync result for device {device}')
        return results

    def results_to_cdb_state(self, results: Dict[str, DeviceCheckSyncResult]):
        """Flush the results dict to CDB operational structures for device automaton

        The results are stored in /ncs:devices/devaut:automaton/device-check-sync-state container"""
        with self.maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
            root = ncs.maagic.get_root(oper_t_write)
            service_log_history_size = root.ncs__devices.global_settings\
                    .devaut__automaton.configuration_consistency_guarantor\
                    .service_log_history_size

            for device_name, device_result in results.items():
                try:
                    service = root.ncs__devices.devaut__automaton[device_name]
                except KeyError:
                    continue
                passive = not service.consistency_guarantor.enabled
                state = service.device_check_sync_state
                state.result = device_result.sync_result
                state.info = device_result.info
                if device_result.sync_result == 'out-of-sync':
                    state.consecutive_out_of_sync_errors += 1
                    if passive:
                        if state.info is None:
                            state.info = 'Device configuration consistency guarantor disabled'
                        else:
                            state.info += ' (Device configuration consistency guarantor disabled)'

                elif device_result.sync_result == 'in-sync':
                    state.consecutive_out_of_sync_errors = 0
                state.last_checked_config = utils.format_yang_date_and_time()

                # write the log
                log_entry = state.log.create(utils.format_yang_date_and_time(device_result.start_timestamp))
                log_entry.end_timestamp = utils.format_yang_date_and_time(device_result.end_timestamp)
                log_entry.duration = device_result.duration
                log_entry.result = state.result
                log_entry.info = state.info

                # prune old entries
                log_keys = state.log.keys()
                if len(log_keys) > service_log_history_size:
                    for log_key in log_keys[:len(log_keys) - service_log_history_size]:
                        del state.log[log_key]

            oper_t_write.apply()

    def results_to_reaction(self, results: Dict[str, DeviceCheckSyncResult]):
        """Take corrective action on negative result

        If the device is out-of-sync, fail the sync-from-performed state on
        the device automaton. If the device is out-of-sync for 5 consecutive
        cycles, stop resyncing and raise an device-configuration-inconsistent
        alarm.

        The reason for dampening the sync-from operation is to prevent an
        infinite cycle due the device changing configuration immediately after
        it's been pushed. An example is normalization of IPv6 addresses."""
        with self.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            ask = alarm_sink.AlarmSink(self.maapi)
            for device_name, device_result in results.items():
                try:
                    service = root.ncs__devices.devaut__automaton[device_name]
                except KeyError:
                    # If the device (automaton) was removed there is no need to
                    # clear the alarm with the code below - all alarms are
                    # pruned when the automaton instance is removed.
                    continue

                mo = "/ncs:devices/devaut:automaton[devaut:device='{}']".format(device_name)
                alarm_id = alarm_sink.AlarmId(device_name, mo, 'devaut:device-configuration-inconsistent', None)
                alarm_text = 'Device configuration consistency returned to normal'
                alarm = alarm_sink.Alarm(alarm_id, alarm_sink.PerceivedSeverity.MAJOR, alarm_text)
                errors = service.device_check_sync_state.consecutive_out_of_sync_errors
                if errors > 4:
                    self.log.debug('{}: {} consecutive out-of-sync-errors'.format(service._path, errors))
                    alarm.alarm_text = 'Device configuration inconsistent with >=5 consecutive out-of-sync errors'
                    ask.submit_alarm(alarm)
                    continue
                else:
                    alarm.cleared = True
                    ask.submit_alarm(alarm)

                passive = service.consistency_guarantor.sync_method is None
                if passive:
                    continue

                # In the normal case the device automaton work function will be
                # triggered by a kicker automatically (observing out-of-sync
                # alarm is-cleared='false'). If an alarm is active this does not
                # happen. Let's trigger the device automaton work function
                # regardless, to be sure it runs at least once.
                if device_result.sync_result != 'in-sync':
                    enqueue_work.action(service, enqueue_work.InternalSource.CHECK_SYNC)
