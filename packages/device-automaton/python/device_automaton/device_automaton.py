import base64
import collections
import datetime
import decimal
import logging
import math
import re
import traceback
from typing import Iterator, List, Optional, Tuple, Mapping, MutableMapping, Callable

import _ncs
import ncs
from ncs.application import Service
from ncs.experimental import Query
from bgworker import background_process
from maagic_copy.maagic_copy import maagic_copy, path_to_xpath
from alarm_sink import alarm_sink
from . import TemplateName
from . import device_credentials
from . import device_management_endpoint
from . import device_detect_ned_id
from . import device_automaton_retrier
from . import device_alarm
from . import service_log
from .device_check_sync import DeviceCheckSync
from . import device_config_consistency_guarantor
from .device_monitor import DeviceMonitor, DeviceMonitorWorker
from .device_automaton_post_commit import PostCommitSubscriber, PostCommitOperPlanSubscriber
from .clear_device_mep_alarm import ClearMepAlarmSubscriber
from .ned_id import NedInfo, find_ned_by_family, get_loaded_neds
from .common import classes, utils
from .maapi_locker import MaapiLocker
from .nso6_trans_conflict_retry import *


def _normalize_state_name(state):
    # make sure our target state has a 'devaut:' prefix. this is is necessary because iterating
    # through list of states using maagic will yield state names with the prefix with NSO >= 4.5
    return 'devaut:' + state if ':' not in str(state) else state


TDevicePlan = MutableMapping[str, MutableMapping[str, Optional[str]]]

class DevicePlan:
    """An abstraction of the device reactive plan

    This helps you manage the reactive service plan of the device automaton since
    it is rather unusual and doesn't follow the standard reactive fastmap
    design pattern of writing the plan from cb_crate(). Instead the plan is
    written directly to oper CDB from various action. This class helps us take
    care of preserving state over time (reading back the data from CDB,
    updating something and then writing back), explicitly delete unused states
    (e.g. if we were to upgrade the service) etc.
    """
    def __init__(self, service):
        self.log = logging.getLogger()
        self.service = service
        self.device_name = service.device
        self.error = None
        self.failed_state = None

        state_diagram = get_state_diagram()
        self.plan: TDevicePlan = collections.OrderedDict([
            ('self', collections.OrderedDict([
                ('ncs:init', 'reached'),
                ('ncs:ready', None)
            ])),
            (service.device, collections.OrderedDict(
                # mypy moans about concatenating lists that contain elements of
                # different types: https://github.com/python/mypy/issues/5492
                # this is a bug, putting the comment here for future developers :)
                [('ncs:init', 'reached')] + [(x, None) for x in state_diagram.keys()] + [('ncs:ready', None)]   # type: ignore
            ))
        ])

        # read in status from CDB
        for component_name, states in self.plan.items():
            for state_name in states:
                try:
                    states[state_name] = service.plan.component[component_name].state[state_name].status
                except KeyError:
                    states[state_name] = 'not-reached'


    def set(self, state, status, error=None):
        """Set a particular component state to the given status

        :param state: name of the state in the plan component
        :param status: status to set
        :param error: optional error message that will be written to the
                      service log and to the error info structure if the state
                      is marked as failed
        """

        # optional error log message, which we can later write to the error
        # info structure from the write method
        if error:
            self.error = error
            self.failed_state = state

        current = self.plan[self.device_name][state]
        if current == status:
            # same value, no-op & return
            return

        self.plan[self.device_name][state] = status
        if status == 'failed':
            marker = False
            for state_name in self.plan[self.device_name]:
                if marker:
                    self.plan[self.device_name][state_name] = 'not-reached'
                if state_name == state:
                    marker = True
            self.plan[self.device_name]['ncs:ready'] = 'failed'
            self.plan['self']['ncs:ready'] = 'failed'
            return

        # set ncs:ready status for both components, device and 'self'
        # get penultimate element in list, it's just before ncs:ready. If it is
        # 'reached', it means all previous states are reached and so we can
        # mark ncs:ready as reached too!
        pen_status = list(self.plan[self.device_name].values())[-2]
        self.plan[self.device_name]['ncs:ready'] = pen_status
        # set ncs:ready for self component - this assume we have a single
        # component for now and copy the above value
        self.plan['self']['ncs:ready'] = pen_status


    def write(self, write_service=None):
        """Write the plan to CDB

        :param write_service: a maagic node to the service in a writable
                              transaction. If the DevicePlan was initialized in
                              a read only transaction we need a write handle
                              for writing the plan..
        """
        if write_service:
            sp = write_service.plan
        else:
            # TODO can we check if this node is writable and raise an exception if it's not?
            # Only when writing "_ncs.error.Error: item is not writable (4)" is raised
            sp = self.service.plan

        for component_name, states in self.plan.items():
            if component_name not in sp.component:
                sp.component.create(component_name)
            comp = sp.component[component_name]
            if component_name == 'self':
                comp.type = 'ncs:self'
            else:
                comp.type = 'devaut:device'

            prev_state = None
            for state_name, status in states.items():
                if state_name not in comp.state:
                    comp.state.create(state_name)
                state = comp.state[state_name]

                changed = state.status != status
                state.status = status or 'not-reached'
                if changed and state.status != 'not-reached':
                    state.when = utils.format_yang_date_and_time()

                # reorder states according to our authoritative plan
                if prev_state is None:
                    comp.state.move(state_name, ncs.maapi.MOVE_FIRST)
                else:
                    comp.state.move(state_name, ncs.maapi.MOVE_AFTER, prev_state)
                prev_state = state_name

            # always set init state to reached
            comp.state['init'].status = 'reached'

            # remove unused states, probably remnants from previous versions of the service
            plan_states = {_normalize_state_name(state_name)
                               for state_name in states}
            cur_states = {state.name for state in sp.component[component_name].state}
            # sets make it easy to find missing states
            to_delete = cur_states - plan_states
            for state_name in to_delete:
                del(sp.component[component_name].state[state_name])

        any_failed = any(status == 'failed' for states in self.plan.values() for status in states.values())
        if any_failed:
            sp.failed.create()

            if self.error:
                error_message = 'Failure in {} state: {}'.format(self.failed_state, self.error)
                service_log.log(write_service or self.service, entry_type='devaut:service-error',
                                level='error', message=error_message)
                error_info = sp.error_info.create()
                error_info.message = error_message
                # TODO: asked how to create instance-identifier in RT31256
                # instance_identifier = _ncs.xpath_pp_kpath(log_entry._path), but it needs a _ncs.HKeypathRef instance
                # error_info.log_entry = instance-identifier path

        else:
            if sp.failed.exists():
                sp.failed.delete()
            if sp.error_info.exists():
                sp.error_info.delete()


# The local classes are quoted here to silence pylint moaning about variables
# not being defined (but they are in fact defined later).
TWorkFunction = Callable[['WorkAction', ncs.maagic.Root, ncs.maagic.Container, ncs.maapi.Maapi], Iterator['WorkStepOutput']]


def get_state_diagram() -> Mapping[str, TWorkFunction]:
    """Return list of states
    """
    state_diagram = collections.OrderedDict([
        ('devaut:management-ip-reachable', WorkAction._setup_reachable_management_ip),
        ('devaut:ssh-host-key-fetched', WorkAction._fetch_ssh_host_key),
        ('devaut:initial-credentials-configured', WorkAction._setup_initial_credentials),
        ('devaut:device-type-detected', WorkAction._detect_device_type),
        ('devaut:commit-queue-handled', WorkAction._handle_commit_queue),
        ('devaut:sync-from-performed', WorkAction._sync_device),
        ('devaut:tricks-performed', WorkAction._perform_tricks),
        ('devaut:management-credentials-configured', WorkAction._setup_management_credentials)
    ])
    return state_diagram


def is_error_transient(message: Optional[str]) -> bool:
    """Based on the retry action and error message, figure out if the failure can be fixed by retrying.

    Right now, this is a very simple blacklist, filtering out known error messages
    that are the result of device errors / NED problems.
    """
    if not message:
        return True

    if 'External error in the NED implementation' in message or \
            'RPC error towards' in message or \
            'No working initial credentials found' in message or \
            re.search(r'^[^ ]*: invalid value', message, re.IGNORECASE):
        return False
    return True


class DeviceAutomaton(ncs.application.Service):
    """Multi-staged device automaton for adding a new device to NSO

    The device automaton manages an NSO device, from its initial creation, credentials management,
    management endpoint monitoring, to making sure the configuration is in sync.

    # Create callbacks
    The mapping logic is responsible for static configuration of the device list entry and
    updating the RFM plan.

    `pre-modification`: the /devices/device list entry is created
    `create`: RFM plan, kickers
    `post-modification`: set the device-ready and re-deploy-trigger leaves

    This looks like a reactive fastmap service but isn't really one. The normal reactive fastmap pattern
    uses early returns from cb_create() and re-deployment of itself to achieve progress over multiple
    transactions. This service instead relies mostly on opening direct transactions from a `work`
    action that performs all of the work beyond what has already been mentioned as part of
    `pre-modification`, `create` and `post-modification`.

    # Work action

    ## Action execution
    The internal `work` action contains the "reactive" logic of the device automaton. The action
    executes a predefined series of steps (plan) for the device. Each step has a pre-check condition
    which allows us to skip unnecessary operations.

    The work action exposes its progress via the standard RFM plan. The internal state for progress
    tracking is stored in CDB. It is imperative that for a single device, there is only one execution
    of the work action at a time.

    This can be guaranteed if direct execution of the `work` action is not permitted. The device
    service exposes an `enqueue-work` action instead, that will increment an internal `work-counter`.

    The transition of the `work-counter` leaf from 0 -> [number greater than 0] will trigger the
    `work` action by a global kicker.
    """

    @Service.create
    # The device automaton manages its own plan in "post_modification" /
    # "work-action", thus the "plan" parameter is not part of "cb_create"
    def cb_create(self, tctx, root, service, proplist):
        self.log.info("device {}".format(service.device))

        # make sure management-credentials are different from initial-credentials
        dc = service.management_credentials
        mgmt_credentials = next(device_credentials.DeviceCredentials.from_config(dc))

        initial_credentials: List[device_credentials.DeviceCredentials] = []
        ned_info = NedInfo.from_cdb(service.ned_id)
        for mep in service.management_endpoint:
            for dc in mep.initial_credentials:
                if str(ned_info.family) == 'cisco-ios-cli' and dc.private_key:
                    raise ValueError("Key based authentication not supported on IOS platform.")
                elif str(ned_info.family) in ('alu-sr-cli', 'ned-sros-yang-nc') and dc.algorithm == 'ssh-ed25519':
                    raise ValueError("ED25519 key type is not supported on Nokia SR OS")
                initial_credentials.extend(
                    device_credentials.DeviceCredentials.from_config(dc))

        if mgmt_credentials in initial_credentials:
            msg = 'ERROR: initial-credentials {} is same as management-credentials. Only set management-credentials to use existing credentials'\
                .format(mgmt_credentials)
            raise Exception(msg)

        if str(ned_info.family) == 'cisco-ios-cli' and mgmt_credentials.private_key:
            raise ValueError("Key based authentication not supported on IOS platform.")
        elif str(ned_info.family) in ('alu-sr-cli', 'ned-sros-yang-nc') and dc.algorithm == 'ssh-ed25519':
            raise ValueError("ED25519 key type is not supported on Nokia SR OS")
        # set up kickers for monitoring alarms related to our device
        template = ncs.template.Template(service)
        template.apply(TemplateName('devaut-kickers-alarms'))
        # set up global kicker for the work function
        template.apply(TemplateName('devaut-kickers-global'))

        dev = root.ncs__devices.device[service.device]

        # copy device automaton settings to the /devices/device entry
        # these are applied through the devaut-devices-device-list-entry template when the device automaton creates a device,
        # but were not applied when an existing device automaton settings were modified
        dev.commit_queue.enabled_by_default = service.commit_queue_behavior.enabled
        if service.accept_any_ssh_host_key:
            dev.ssh.host_key_verification = 'none'
        else:
            dev.ssh.host_key_verification = None

        # copy the ssh-algorithms container to the /devices/device entry for NSO 5.6+
        try:
            ncs.maagic.get_trans(service).shared_copy_tree(service.ssh_algorithms._path, dev.ssh_algorithms._path)
        except AttributeError:
            pass

        self.log.debug('Device automaton done for {}'.format(service.device))
        return proplist


    def _create_device(self, root: ncs.maagic.Root, service: ncs.maagic.Container, device: str):
        if root.devices.device.exists(device):
            self.log.debug('list entry already created, skipping...')
        else:
            # grab the first configured management endpoint
            try:
                first_management_endpoint = next(service.management_endpoint.__iter__())
                address = first_management_endpoint.address
                port = first_management_endpoint.port
            except StopIteration:
                # No management endpoint known, use dummy for now
                address = 'dummy'
                port = ''

            # If the authgroup service already exists then force a re-deploy to
            # ensure the authgroup exists too. This allows us to recover in
            # case someone removes the device and authgroup without the
            # automatons knowledge.
            authgroups = root.ncs__devices.ncs__authgroups.devaut__authgroup
            if device in authgroups:
                authgroups[device].private.re_deploy_counter += 1

            dc = next(device_credentials.DeviceCredentials.from_config(service.management_credentials))
            tvars = utils.prepare_template_variables(
                {'NAME': device, 'USERNAME': dc.username, 'PASSWORD': dc.password,
                 'PRIVATE_KEY': dc.private_key})
            template = ncs.template.Template(service)
            template.apply(TemplateName('devaut-authgroup-service'), tvars)
            ned_info = NedInfo.from_cdb(service.ned_id)
            self.log.info(f'ned-info {ned_info} netconf={ned_info.netconf}')
            if ned_info.identity.abstract is False:
                ned_id = str(ned_info.identity)
            elif ned_info.netconf is True:
                ned_id = 'netconf'
            else:
                candidate = find_ned_by_family(get_loaded_neds(root), ned_info.family)
                if candidate is not None:
                    self.log.info(f'Picked ned-id {candidate} as best fit for family {ned_info}')
                    ned_id = str(candidate.identity)
                else:
                    raise ValueError(f'No suitable ned-id found for family {ned_info.family}')

            tvars = utils.prepare_template_variables({
                'MGMT_ADDRESS': address,
                'MGMT_PORT': port,
                'AUTHGROUP': device,
                'NED_ID': ned_id,
                'NETCONF': ned_info.netconf,
                'GENERIC': ned_info.generic
            })
            template.apply(TemplateName('devaut-devices-device-list-entry'), tvars)
            self.log.debug('{}: creating new device {}'.format(service._path, device))


    @Service.pre_modification
    def cb_pre_modification(self, tctx, op, kp, root, proplist):
        self.log.debug('In service pre_modification for ', kp)

        device = str(kp[0][0])

        if op in (ncs.dp.NCS_SERVICE_CREATE, ncs.dp.NCS_SERVICE_UPDATE):
            service = root.ncs__devices.devaut__automaton[device]
            self._create_device(root, service, device)

        if op == ncs.dp.NCS_SERVICE_UPDATE:
            service = root.ncs__devices.devaut__automaton[device]
            with ncs.maapi.single_read_trans('python-devaut-service-pre-modification', 'system') as t_read:
                orig_service = ncs.maagic.get_node(t_read, kp)
                if orig_service.ned_id != service.ned_id:
                    ned_info = NedInfo.from_cdb(service.ned_id)
                    if ned_info.identity.abstract:
                        raise Exception('Changing ned-id to abstract identity is not supported')

        return proplist

    @Service.post_modification
    def cb_post_modification(self, tctx, op, kp, root, proplist):
        self.log.debug('In service post_modification for ', kp)

        device = str(kp[0][0])

        if op in (ncs.dp.NCS_SERVICE_CREATE, ncs.dp.NCS_SERVICE_UPDATE):
            service = root.ncs__devices.devaut__automaton[device]

            # Write the plan. If this is in create, the first time the service
            # is created, then we will write an "empty" plan, where the status
            # of every state will be 'not-reached'. The DevicePlan will always
            # ensure the correct order of states and that the correct states
            # exist, i.e. if states are added or removed, this will ensure that
            # the plan is updated according to the authoritative source here in
            # the code.
            device_plan = DevicePlan(service)
            device_plan.write()

        return proplist


class EnqueueWorkAction(ncs.dp.Action):
    @ncs.dp.Action.action
    @retry_on_conflict()
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        self.log.debug(f'{kp}: enqueue-work: kicker-id={action_input.kicker_id} path={action_input.path}')
        # We intentionally start a write transaction towards *running* and not
        # operational DB. The work-count leaf is oper data, but we create a
        # kicker in the service create callback that monitors the leaf. If the
        # work-count leaf is bumped from 0 to 1 too fast (before the kicker is
        # activated) the work action will not run. The root cause for this race
        # condition is the fact that CDB subscribers hook into the transaction
        # engine and receive the change notifications *before* the transaction
        # is completed. If the subscriber post_iterate callback is fast enough
        # it is able to open an oper-write transaction to modify data while the
        # first write transaction still has the lock. This happens because
        # oper-write transactions do not acquire the lock.
        with ncs.maapi.single_write_trans('python-devaut-service-enqueue-work', 'system') as t_write:
            service = ncs.maagic.get_node(t_write, kp)
            service.work_count += 1
            action_output.success = True
            action_output.message = 'Current work-count: {}'.format(service.work_count)
            if action_input.kicker_id:
                message = f'Service retried by kicker {action_input.kicker_id} (path={action_input.path})'
            elif action_input.internal_source:
                message = f'Service retried by internal component {action_input.internal_source}'
            else:
                message = 'Service retry source unknown (manual?)'
            service_log.log(service, entry_type='devaut:service-retried', level='debug', message=message)

            try:
                t_write.apply()
            except Exception as e:
                self.log.error(f'{kp}: enqueue-work: error increasing work-count: {e}')
                action_output.success = False
                action_output.message = f'Error increasing work-count: {e}'
            else:
                action_output.success = True


class WorkStepOutput:
    """DTO for the work function step outputs.

    The individual steps use one of the static methods defined below to create an instance
    with the data that makes sense for given state.
    These results are then mapped to RFM plan states:
    - ok -> reached: the step was completed successfully
    - fail -> failed: the step has failed
    - work -> not-reached: the pre-check function has determined the step is not completed yet
    """
    def __init__(self, success=False, message=None, working=False):
        self.success = success
        self.message = message
        self.working = working

    @staticmethod
    def ok():
        return WorkStepOutput(success=True)

    @staticmethod
    def fail(message):
        return WorkStepOutput(success=False, message=str(message))

    @staticmethod
    def work():
        return WorkStepOutput(working=True)

    def __str__(self):
        if self.working:
            return '(working)'
        elif self.success:
            return '(ok)'
        else:
            return '({}, {})'.format('fail', self.message)


class DeviceRemovedException(Exception):
    pass


def device_removed_aborter(cb_action):
    def action_wrapper(self, uinfo, name, kp, action_input, action_output):
        try:
            cb_action(self, uinfo, name, kp, action_input, action_output)
        except Exception as e:
            if isinstance(e, DeviceRemovedException) or classes.device_automaton_removed(e):
                message = 'Device is in the process of being removed, aborting work function'
                self.log.warning(f'{kp}: {message}')
            elif classes.device_entry_removed(e):
                message = '/devices/device list entry missing, re-deploying the automaton service instance'
                self.log.info(f'{kp}: {message}')
                with utils.maapi_session('python-devaut-re-deploy', 'system') as m:
                    ncs.maagic.get_node(m, kp).reactive_re_deploy()
            else:
                with ncs.maapi.single_read_trans('python-devaut-recon', 'system') as t:
                    if not t.exists(str(kp)):
                        message = 'Device is in the process of being removed, aborting work function'
                        self.log.warning(f'{kp}: {message}')
                    else:
                        message = f'Unhandled error in {kp}: {type(e).__name__}: {e}'
                        self.log.error(f'{kp}: {message}')
                        self.log.debug(traceback.format_exc())
            action_output.success = False
            action_output.message = message
    return action_wrapper


class WorkAction(ncs.dp.Action, classes.DeviceTimeoutTaker, classes.Retrier):
    """device automaton "big function" that does all the work

    This action is an entry point to the device automaton worker. To see the steps performed by
    the worker, see the get_state_diagram() method.

    Only a single instance of this action may run for a device (concurrency=1). Internal state is
    kept in CDB and multiple actions that work on the same FSM are not synchronised, they could
    locally assume different states and try to transition to another state - all concurrently.

    The action is triggered when the `work-count` leaf on the device automaton is increased from 0 to
    a non-zero value. Upon entering the function, if multiple work requests are pending (work-count
    > 1), the function will only complete a single pass. If another work request is added while the
    action is running, the function will make a second pass after the current is completed and
    continue until the work-count leaf reaches 0. Work is coalesced, so if multiple work requests
    are added while the action is running, only one (1) extra pass will be performed.

    Users and other components may trigger the function through the `enqueue-work` action.
    """
    @ncs.dp.Action.action
    @device_removed_aborter
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        """The main action that controls the work
        Start reading code here.
        """
        if action_input.kicker_id is None:
            # this guy better know what he's doing ...
            self.log.critical('{}: executed work action directly'.format(kp))

        device_name = str(kp[0][0])
        state_diagram = get_state_diagram()

        success = None
        message = None
        start_time = datetime.datetime.utcnow()
        loop_count = 0

        with utils.maapi_session(f'python-devaut-service-work-{device_name}', 'system') as m:
            def _some_work() -> bool:
                """Check if there is work to do, consolidate multiple requests into one

                Prevent this instance of the action from running for too long by checking the start time
                and keeping a loop count."""
                nonlocal loop_count
                do_work = False
                with m.start_write_trans(db=ncs.OPERATIONAL) as _oper_t_write:
                    _write_service = ncs.maagic.get_node(_oper_t_write, kp)
                    self.log.debug(f'{kp}: work-count: {_write_service.work_count}')
                    if _write_service.work_count >= 1:
                        _write_service.work_count = 1
                        if loop_count >= 5 or \
                           start_time + datetime.timedelta(minutes=10) < datetime.datetime.utcnow():
                            _write_service.work_count = 0
                            self.log.debug(f'{kp}: breaking possible work action loop')
                        else:
                            do_work = True
                        loop_count += 1
                        _oper_t_write.apply()
                return do_work

            # If a new work function is enqueued while this is running,
            # repeat the execution loop.
            while _some_work():
                break_flag = False

                # The progress of the work function execution is exposed to
                # the users via the device automaton RFM plan. The DevicePlan
                # helper is used to update the plan, following the rules of
                # state behavior.
                with m.start_read_trans(db=ncs.OPERATIONAL) as oper_t_read:
                    service = ncs.maagic.get_node(oper_t_read, kp)
                    device_plan = DevicePlan(service)

                # Execute all step functions. Every step function yields progress updates
                # via the WorkStepOutput result - it is a generator function.
                # The three possible outcomes are:
                # - work: pre-check has detected this step needs to be executed
                # - ok: pre-check has detected this step can be skipped, or has just been completed
                # - fail: this step has failed
                for state, fn in state_diagram.items():
                    _ncs.dp.action_set_timeout(uinfo, 120)
                    step_start_time = datetime.datetime.utcnow()
                    try:
                        # All step functions must output at least one WorkStepOutput result.
                        # If for whatever reason they don't, set result here to make sure it is
                        # defined.
                        result = WorkStepOutput.fail('No output from state {}'.format(state))

                        # Collect all outputs from the work function. The last value stored in
                        # the result variable is the result of the work function (ok / fail).
                        # We start a separate MAAPI read transaction for every step to not worry
                        # about cache invalidation.
                        with m.start_read_trans() as t_read:
                            root = ncs.maagic.get_root(t_read)
                            service = ncs.maagic.get_node(t_read, service)
                            for result in fn(self, root, service, m):
                                self.log.debug('Got result {} for device {} with state {}'.format(result, kp, state))
                                if result.working:
                                    device_plan.set(state, 'not-reached')

                                    with m.start_write_trans() as t_write:
                                        write_service = ncs.maagic.get_node(t_write, kp)
                                        device_plan.write(write_service)
                                        t_write.apply()

                    except Exception as e:
                        success = False
                        if classes.device_automaton_removed(e):
                            raise DeviceRemovedException()
                        elif classes.device_entry_removed(e):
                            message = '/devices/device list entry missing, re-deploying the automaton service instance'
                            self.log.info(f'{kp}: {message}')
                            ncs.maagic.get_node(m, kp).reactive_re_deploy()
                        else:
                            with ncs.maapi.single_read_trans('python-devaut-recon', 'system') as t:
                                if not t.exists(str(kp)):
                                    raise DeviceRemovedException()
                                else:
                                    message = f'Unhandled error in {kp}: {type(e).__name__}: {e}'
                                    self.log.error(f'{kp}: {message}')
                                    self.log.debug(traceback.format_exc())
                    else:
                        success = result.success
                        message = result.message

                    finally:
                        step_stop_time = datetime.datetime.utcnow()
                        state_status = 'reached' if success else 'failed'
                        self.log.debug('Got final (success, message) = ({}, {}) for device {}, state = {} in {} seconds'
                                        .format(success, message, kp, state, (step_stop_time-step_start_time).total_seconds()))
                        device_plan.set(state, state_status, error=message)

                        with m.start_write_trans() as t_write:
                            write_service = ncs.maagic.get_node(t_write, kp)
                            device_plan.write(write_service)
                            t_write.apply()

                        if not success:
                            # `break`ing in try..finally will swallow unhandled exceptions
                            # https://docs.python.org/3.6/reference/compound_stmts.html#finally
                            break_flag = True
                    if break_flag:
                        # stop the current work iteration
                        break

                # All step functions have executed, or there was an error
                with m.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                    write_service = ncs.maagic.get_node(oper_t_write, kp)
                    # success == True if all steps have completed successfully
                    if success:
                        write_service.device_ready = True
                        if write_service.re_deploy_needed:
                            write_service.re_deploy_trigger = True
                            write_service.re_deploy_needed = False
                            service_log.log(write_service, 'devaut:service-re-deploy-triggered', 'info',
                                            'Dependent services will be re-deployed')
                    if write_service.work_count > 0:
                        write_service.work_count -= 1
                    oper_t_write.apply()

            # All work completed - work-count reached 0
            stop_time = datetime.datetime.utcnow()
            work_duration = (stop_time - start_time).total_seconds()

            # Calculate the exponential backoff (up to max-backoff)
            with m.start_read_trans() as t_read:
                dsr = ncs.maagic.get_node(t_read, '/ncs:devices/global-settings/devaut:automaton/retrier')
                dsr_enabled = dsr.enabled
                if dsr_enabled:
                    max_backoff = dsr.failure_detection.max_backoff
                    service_xpath = path_to_xpath(ncs.maagic.get_node(t_read, kp))

            if dsr_enabled:
                raise_alarm = False
                with m.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                    write_service = ncs.maagic.get_node(oper_t_write, kp)
                    if success is False:
                        # If the work function resulted in failure, check for error type and
                        # increase backoff
                        backoff = write_service.service_retrier.backoff_period or \
                                    math.ceil(work_duration / 60)
                        backoff = min(2 * backoff, max_backoff)

                        error_transient = is_error_transient(message)
                        if not error_transient:
                            # for non-transient errors (NED issues, config errors), check if we
                            # have seen the error message before. if so, immediately set backoff
                            # to max-backoff and raise an alarm

                            # we could preprocess the message here to perform a fuzzy match?
                            if message == write_service.service_retrier.previous_message:
                                backoff = max_backoff
                                raise_alarm = True
                            write_service.service_retrier.previous_message = message

                        log_message = 'failed to complete work action in {}{}. Transient: {}{} Backoff: {}' \
                            .format(work_duration, ', error ' + message if message else '',
                                    error_transient, ' (possible loop detected)' if raise_alarm else '',
                                    datetime.timedelta(minutes=backoff))
                        self.log.warning('{}: {}'.format(kp, log_message))
                        service_log.log(write_service, entry_type='devaut:service-retried', level='error', message=log_message)

                        write_service.service_retrier.backoff_period = backoff
                        disabled_until = stop_time + datetime.timedelta(minutes=backoff)
                        write_service.service_retrier.disabled_until = utils.format_yang_date_and_time(disabled_until)
                    else:
                        # Reset backoff timers if the work function ran successfully
                        write_service.service_retrier.delete()
                    oper_t_write.apply()

                ask = alarm_sink.AlarmSink(m)
                alarm_id = alarm_sink.AlarmId(device_name, service_xpath,
                                              type='devaut:service-retrier-loop',
                                              specific_problem=None)
                alarm = alarm_sink.Alarm(alarm_id,
                                            severity=alarm_sink.PerceivedSeverity.WARNING,
                                            alarm_text=None, impacted_objects=[str(kp)])
                if raise_alarm:
                    alarm.alarm_text = 'Loop detected: {}'.format(message)
                else:
                    alarm.alarm_text = 'Service retried successfully.'
                    alarm.cleared = True
                ask.submit_alarm(alarm)

        action_output.success = success
        action_output.message = message


    def _setup_reachable_management_ip(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Setup the address to which we should connect to the device
        This determines which one out of the configured management-endpoint are alive should be used
        to communicate with the device and subsequently configures that on the device.
        """
        # pre-check, which consists of checking if the used address to connect
        # to the device is the first of the configured management-endpoints,
        # i.e. the most desirable and if we can actually successfully connect
        # to the device. If both are true we can immediately exit and if not
        # then we need to do further work.
        device = root.ncs__devices.device[service.device]
        first_management_endpoint = next(service.management_endpoint.__iter__())

        if device.address == first_management_endpoint:
            res = device.connect()
            if res.result is True:
                yield WorkStepOutput.ok()
                return

        yield WorkStepOutput.work()

        self._take_timeout(service.device, service)
        dm = DeviceMonitor(self.log, maapi)
        # quickly refresh management endpoint stats just for this device
        monitor_result = dm.monitor(device_filter=lambda x: x.device == service.device)
        result = dm.reconfigure_device_management_endpoint(service.device)

        if result[service.device].management_endpoint is None:
            # The monitor_result var contains a
            # dict(Device(name='bob', management_endpoint=...): Optional[ManagementEndpoint()])
            # with a single entry - we're executing the action on a particular device.
            # Let's look at the key, to retrieve the list of checked management endpoints
            device_result = next(iter(monitor_result.keys()))
            tried_meps = ', '.join('{}:{}'.format(mep.address, mep.port) for mep in device_result.management_endpoint)
            yield WorkStepOutput.fail('No management endpoints alive (tried: {})'.format(tried_meps))
        else:
            yield WorkStepOutput.ok()


    def _fetch_ssh_host_key(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Fetch the current SSH host key from the device.
        The SSH key is persisted in NSO in the standard /devices/device/ssh/host-key location, but
        also in the list of management endpoints. The latter allows us to detect when the key has
        changed.
        """
        # pre-check, which consists of checking if the service oper leaf ssh-host-key-fetch-needed
        # (set by device monitor on management endpoint change or detected missing host key) is set
        device = root.ncs__devices.device[service.device]

        if not service.alive:
            yield WorkStepOutput.fail('Device has no reachable management endpoints')
            return

        if not service.ssh_host_key_fetch_needed or len(device.ssh.host_key) > 0:
            yield WorkStepOutput.ok()
            return

        yield WorkStepOutput.work()

        current_mep = next(device_management_endpoint.ManagementEndpoint(mep.address, mep.port)
                           for mep in service.management_endpoint if mep.alive)
        # SSH host-key handling:
        # 1.    If management endpoint was updated or we don't have SSH host-key for the
        #       device yet, execute ssh/fetch-host-key action.
        # 2.    Check if we have stored the host-key for the current management endpoint.
        # 2.1.  If yes, check if it matches the current key. On mismatch raise alarm??
        # 3.    Store the current host-key for the management endpoint

        # stupid Cisco XR SSH connection rate-limiter :/
        self._take_timeout(service.device, service)
        _, (result, info) = self._retry_function(device.ssh.fetch_host_keys, lambda x: (x.result != 'failed', (x.result, x.info)),
                                                 max_retries=1, suffix='ssh/fetch-host-keys on {}'.format(device._path))
        if result == 'updated':
            self.log.info('{}: SSH host key fetched for management endpoint {}'
                          .format(device._path, current_mep))
        elif result == 'unchanged':
            self.log.debug('{}: SSH host key unchanged for management endpoint {}'
                           .format(device._path, current_mep))
        else:
            # error?
            message = 'SSH host key fetch result {}: {}'.format(result, info)
            self.log.warning('{}: {}'.format(device._path, message))
            yield WorkStepOutput.fail(message)
            return

        # clear maagic node cache after executing ssh/fetch-host-keys action
        device.ssh.host_key._set_attr('_populated', None)

        mep = service.management_endpoint[current_mep.address]
        # prepare dictionaries with existing data
        mep_keys = {key_algorithm[0]: mep.host_key[key_algorithm].key_data
                    for key_algorithm in mep.host_key.keys()}
        # base64-encode the keys from the device tree
        dev_keys = {key_algorithm[0]: base64.b64encode(device.ssh.host_key[key_algorithm].key_data).decode()
                    for key_algorithm in device.ssh.host_key.keys()}

        # have we stored host keys for this management endpoint yet?
        if len(mep_keys) > 0:
            # let's see if the current host keys are different than the ones we remember!
            for key_algorithm in set.union(set(mep_keys), set(dev_keys)):

                if key_algorithm not in mep_keys or key_algorithm not in dev_keys \
                        or mep_keys[key_algorithm] != dev_keys[key_algorithm]:
                    msg = 'host key {} mismatch: {} != {}'\
                        .format(key_algorithm, dev_keys.get(key_algorithm, None),
                                mep_keys.get(key_algorithm, None))
                    self.log.warning('{}: {}'.format(device._path, msg))
        else:
            # "sticky" the current host key for the management endpoint
            self.log.info('{}: persisting SSH host keys for management endpoint {}'
                          .format(device._path, current_mep))
            with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                root = ncs.maagic.get_root(oper_t_write)
                mep = root.ncs__devices.devaut__automaton[service.device].management_endpoint[current_mep.address]
                for key_algorithm, key_data in dev_keys.items():
                    key_mep = mep.host_key.create(key_algorithm)
                    key_mep.key_data = key_data
                oper_t_write.apply()

        yield WorkStepOutput.ok()


    def _setup_initial_credentials(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Setup the initial credentials to use for the device
        The initial credentials is the first set of credentials we use to authenticate towards the
        device with. Once we've been able to authenticate and log in, we create the management
        credentials and switch to using them.
        """
        device = service.device

        # check if the credentials currently configured on the device work
        dcj = device_credentials.DeviceCredentialJuggler(root, maapi, self.log)
        success, _ = dcj.test_credentials(service, root.devices.device[device])
        if success:
            yield WorkStepOutput.ok()
            return

        yield WorkStepOutput.work()

        success, attempted_credentials = dcj.setup_initial_device_credentials(device)
        if not success:
            msg = 'No working initial credentials found, tried: {}'\
                .format(', '.join('{}: {}'.
                                  format(dc, fail_reason) for dc, fail_reason in attempted_credentials.items()))
            self.log.warning(msg)
            yield WorkStepOutput.fail(msg)
            return
        yield WorkStepOutput.ok()


    def _detect_device_type(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Detect the device-type and write the OS as a cached value for fast and easy lookup later on
        """
        yield WorkStepOutput.work()
        device = service.device
        ncs_device = root.devices.device[device]

        if len(ncs_device.capability) == 0:
            yield WorkStepOutput.fail('No capabilities found on device')
            return

        needs_redeploy = False

        alarms = {}
        candidate_ned_id = None
        ned_info = NedInfo.from_cdb(service.ned_id)
        current_ned_id = utils.read_ned_id(ncs_device)
        if not ned_info.identity.abstract and service.ned_id != current_ned_id:
            candidate_ned_id = service.ned_id
            self.log.info(f'Configured ned-id {service.ned_id} is different than currently used ned-id {current_ned_id}, will migrate')
            with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                device_automaton = ncs.maagic.get_node(oper_t_write, service._path)
                device_automaton.auto_ned_id.candidate = service.ned_id
                oper_t_write.apply()
        elif ned_info.netconf and ned_info.identity.abstract:
            # The NSO built-in 'netconf' ned-id (supporting essentially just a
            # NETCONF session) is a derived identity from 'netconf-ned-id'.
            # This means that if we used the 'netconf' identity for NED
            # filtering (= only including ned-ids derived from a given base),
            # we would *not* see any of the loaded NETCONF NEDs!
            if str(ned_info.identity) == 'ned:netconf':
                ned_filter = None
            else:
                ned_filter = ned_info.identity
            ned_results = device_detect_ned_id.match_devices_to_ned_ids(root, device, ned_filter)

            # dump the results of matching capabilities to CDB
            with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                device_automaton = ncs.maagic.get_node(oper_t_write, service._path)
                device_automaton.auto_ned_id.detection_results.delete()
                for n, s in ned_results[device].items():
                    r = device_automaton.auto_ned_id.detection_results.create(n)
                    # the score leaf has a precision of 5 decimal places
                    r.score = s.quantize(decimal.Decimal('1.00000'))
                oper_t_write.apply()

            candidate_ned_id = device_detect_ned_id.find_best_ned_id(ned_results)[device]
            if candidate_ned_id:
                score = ned_results[device][candidate_ned_id]
                self.log.debug(f'Found ned-id {candidate_ned_id} with score {score}')
                settings = root.ncs__devices.ncs__global_settings.devaut__automaton.ned_id_detection
                candidate_family = str(NedInfo.from_cdb(candidate_ned_id).family)
                if candidate_ned_id in settings.exception:
                    threshold = decimal.Decimal(settings.exception[candidate_ned_id].minimal_score)
                elif candidate_family in settings.exception:
                    threshold = decimal.Decimal(settings.exception[candidate_family].minimal_score)
                else:
                    threshold = decimal.Decimal(settings.minimal_score)
                if score < threshold:
                    alarms['ned-id-not-found'] = {
                        'text': f'Not using {candidate_ned_id} with score {score} below threshold {threshold}',
                        'cleared': False
                    }
                    candidate_ned_id = None
                else:
                    alarms['ned-id-not-found'] = {
                        'text': 'Suitable NED found',
                        'cleared': True
                    }
            else:
                alarms['ned-id-not-found'] = {
                    'text': 'No suitable NED found',
                    'cleared': False
                }

            # The candidate_ned_id variable either contains a ned-id with
            # sufficient score above the threshold or it was set to None
            with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                device_automaton = ncs.maagic.get_node(oper_t_write, service._path)
                device_automaton.auto_ned_id.candidate = candidate_ned_id
                oper_t_write.apply()

        if candidate_ned_id and current_ned_id != candidate_ned_id:
            if ned_info.identity.abstract and current_ned_id != 'ned:netconf':
                # Candidate ned-id is something different than currently configured,
                # but we're not going to perform automatic migration for NETCONF!
                alarms['candidate-ned-id-changed'] = {
                    'text': f'New ned-id detected: {candidate_ned_id} (current {current_ned_id})',
                    'cleared': False
                }
            else:
                alarms['candidate-ned-id-changed'] = {
                    'text': 'Will migrate to new candidate ned-id',
                    'cleared': True
                }
                err = self._migrate(ncs_device, service, maapi, candidate_ned_id)
                if err is not None:
                    alarms['ned-id-migrate-error'] = {
                        'text': err,
                        'cleared': False
                    }
                else:
                    alarms['ned-id-migrate-error'] = {
                        'text': 'ned-id migrated successfuly',
                        'cleared': True
                    }
                    needs_redeploy = True

        # Collect messages from raised alarms, to also be added as failure
        # reasons in the service log
        errors = []

        # Set or clear the three alarms - 'ned-id-not-found',
        # 'candidate-ned-id-changed' and 'ned-id-migrate-error'.
        ask = alarm_sink.AlarmSink(maapi)
        service_xpath = path_to_xpath(service)
        for alarm_type in alarms:
            alarm_id = alarm_sink.AlarmId(device, service_xpath,
                                        type=alarm_type,
                                        specific_problem=None)
            alarm = alarm_sink.Alarm(alarm_id, severity=alarm_sink.PerceivedSeverity.MINOR,
                    alarm_text=alarms[alarm_type]['text'], impacted_objects=[str(service._path)])
            alarm.cleared = alarms[alarm_type]['cleared']
            ask.submit_alarm(alarm)
            if not alarm.cleared:
                errors.append(alarm.alarm_text)

        if errors:
            self.log.error('\n'.join(errors))
            yield WorkStepOutput.fail('\n'.join(errors))
            return

        yield WorkStepOutput.ok()

    def _migrate(self, ncs_device: ncs.maagic.Container, service: ncs.maagic.Container, maapi: ncs.maapi.Maapi, ned_id: str) -> Optional[str]:
        """Helper for running the /devices/device/migrate action

        If successful the NSO built-in action changes the ned-id in the
        device-type container.

        If migration fails an error message is returned (to be used in alarm)."""
        self.log.debug(f'Migrating ned-id {ncs_device.device_type.netconf.ned_id} to {ned_id}')
        migrate_input = ncs_device.migrate.get_input()
        migrate_input.new_ned_id = ned_id
        migrate_input.wait_for_lock.create()
        migrate_input.verbose.create()
        try:
            migrate_output = ncs_device.migrate(migrate_input)
        except Exception as e:
            msg = f'Migration to ned-id {ned_id} failed with error: {e}'
            self.log.error(msg)
            return msg

        try:
            affected_services = migrate_output.affected_services.as_list()
        except AttributeError:
            # 6.1+ moved affected-services to a list within the path
            affected_services = []
            for mp in migrate_output.modified_path:
                affected_services.extend(affected_service.id for affected_service in mp.affected_service)
        self.log.debug(f'Migration successful, affected services: {affected_services}')

        # Upon a successful ned-id migration an implicit sync-from was
        # performed by NSO. The next step is to re-deploy the services using
        # this device.
        with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
            service_write = ncs.maagic.get_node(oper_t_write, service._path)
            service_write.re_deploy_needed = True
            oper_t_write.apply()
        return None

    def _handle_commit_queue(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Find failed commit queue items and handle (=prune) them
        """
        device = service.device
        ncs_device = root.devices.device[device]

        queue_length = ncs_device.commit_queue.queue_length
        if queue_length is None or queue_length == 0:
            yield WorkStepOutput.ok()
            return
        else:
            yield WorkStepOutput.work()

        # find the first active commit-queue item
        with Query(ncs.maagic.get_trans(root),
                f"ncs:queue-item[1][ncs:devices=('{device}')]",
            '/ncs:devices/ncs:commit-queue', ['id'],
            result_as=ncs.QUERY_STRING) as q:
            try:
                item_id = int(next(q)[0])
            except StopIteration:
                # the queue item may be gone in the time window between
                # checking the queue-length and retrieving it
                self.log.debug('Could not find the first commit-queue item')
                yield WorkStepOutput.ok()
                return

        queue_item = root.ncs__devices.ncs__commit_queue.ncs__queue_item[item_id]
        # Nothing to do here if the item is currently executing. There is a
        # lock on the device anyway so any changes pushed to the device while
        # the item is executing (like everything the automaton does uses
        # COMMIT_QUEUE_BYPASS) will be pushed afterwards.
        if queue_item.status == 'executing':
            yield WorkStepOutput.ok()
            return

        if queue_item.status == 'locked' and not service.commit_queue_behavior.auto_recovery:
            yield WorkStepOutput.fail(f'Locked queue-item {item_id} found, but commit queue auto-recovery disabled for service')
            return

        if queue_item.status == 'locked':
            self.log.debug(f'Pruning commit-queue items for device {device}')
            prune_action = root.ncs__devices.ncs__commit_queue.ncs__prune
            prune_input = prune_action.get_input()
            prune_input.device.create(device)
            prune_result = prune_action(prune_input)
            self.log.debug(f'Prune result for device {device}: num-matched-queue-items={prune_result.num_matched_queue_items}, num_deleted_queue_items={prune_result.num_deleted_queue_items}')

            self.log.debug(f'Unlocking device {device} queue-item {item_id}')
            root.ncs__devices.ncs__commit_queue.ncs__queue_item[item_id].unlock()

            with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                service_write = ncs.maagic.get_node(oper_t_write, service._path)
                service_log.log(service_write, 'devaut:service-state-changed', 'info', f'Unlocked commit-queue item {item_id} and pruned others')
                oper_t_write.apply()

            yield WorkStepOutput.ok()
        else:
            yield WorkStepOutput.fail(f'Unhandled queue-item status: {queue_item.status}')

    def _sync_device(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Check if device is out-of-sync and run appropriate sync action
        """
        sync_state = service.device_check_sync_state
        initial_sync = True if sync_state.result == 'unknown' or sync_state.result is None else False

        if initial_sync:
            sync_method = 'sync-from'
        else:
            sync_method = service.consistency_guarantor.sync_method

        # If we already sync'ed once and automatic out-of-sync remediation is disabled,
        # there is nothing to do here
        if sync_method is None and not initial_sync:
            self.log.debug('automatic out-of-sync remediation disabled, skipping ...')
            yield WorkStepOutput.ok()
            return

        # check if we are out-of-sync, if not, we can return early
        dcs = DeviceCheckSync(maapi, self.log)
        device = service.device
        results = dcs.device_check_sync([device])
        if results[device].sync_result == 'in-sync':
            self.log.debug('already in sync, skipping...')
            yield WorkStepOutput.ok()
            return
        else:
            yield WorkStepOutput.work()

        self._take_timeout(device, service)
        result: Tuple[bool, Optional[str]] = (True, None)
        ncs_device = root.ncs__devices.device[device]
        if not (initial_sync or sync_method == 'sync-from'):
            result = self._retry_function(ncs_device.sync_to, lambda x: (x.result, x.info), max_retries=1,
                                          suffix='{} on {}'.format('sync-to', service._path))
        if not result[0] or initial_sync or sync_method == 'sync-from':
            result = self._retry_function(ncs_device.sync_from, lambda x: (x.result, x.info), max_retries=1,
                                          suffix='{} on {}'.format('sync-from', service._path))

            # Upon a successful sync-from, dependent services need to be re-deployed. Let's remember
            # that in the results/re-deploy-needed oper leaf.
            if result[0]:
                with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                    service_write = ncs.maagic.get_node(oper_t_write, service._path)
                    service_write.re_deploy_needed = True
                    oper_t_write.apply()

        if not result[0]:
            self.log.warning('Could not sync config from device {}: {}'
                             .format(device, result[1]))
            yield WorkStepOutput.fail(result[1])
        else:
            yield WorkStepOutput.ok()


    def _perform_tricks(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Perform device platform specific tricks.
        For Nokia SROS_CLI we have to configure the rollback path before we can use the transactional
        candidate configuration facilities, so this is performed here as a device specific trick.

        For Juniper Junos version 15 we have to configure /system/services/netconf/rfc-compliant
        in two stages - 1. configure, the transaction will fail but succeed on device 2. Sync from
        device to ensure device is in sync and rfc-compliant statement also ends in ncs.
        """
        device = service.device
        ncs_device = root.devices.device[device]
        capas = ncs_device.capability

        commit_params = ncs.maapi.CommitParams()
        commit_params.commit_queue_bypass()

        if 'http://tail-f.com/ned/alu-sr' in capas:
            # check if tricks were previously performed on this device?
            if ncs_device.config.alu__system.rollback.rollback_location == 'cf3:/rollback':
                self.log.debug('tricks already performed on alu-sr, skipping...')
                yield WorkStepOutput.ok()
                return

            yield WorkStepOutput.work()
            def alu_step1(t_write):
                write_root = ncs.maagic.get_root(t_write)
                write_ncs_device = write_root.devices.device[device]
                write_ncs_device.ned_settings.alu_meta__alu_sr.candidate_commit = 'disabled'
                return True
            maapi.run_with_retry(alu_step1, commit_params=commit_params)

            def alu_step2(t_write):
                write_root = ncs.maagic.get_root(t_write)
                write_ncs_device = write_root.devices.device[device]
                write_ncs_device.config.alu__system.rollback.rollback_location = 'cf3:/rollback'
                return True
            maapi.run_with_retry(alu_step2, commit_params=commit_params)

            def alu_step3(t_write):
                write_root = ncs.maagic.get_root(t_write)
                write_root.devices.device[device].ned_settings.alu_meta__alu_sr.candidate_commit = 'enabled'
                return True
            maapi.run_with_retry(alu_step3, commit_params=commit_params)

        elif 'http://tail-f.com/ned/huawei-vrp' in capas:
            # check if tricks were previously performed on this device?
            if ncs_device.config.vrp__user_security_policy.enable is False:
                yield WorkStepOutput.ok()
                return

            yield WorkStepOutput.work()

            def vrp_step1(t_write):
                write_root = ncs.maagic.get_root(t_write)
                write_ncs_device = write_root.devices.device[device]
                write_ncs_device.config.vrp__user_security_policy.enable = False
                return True
            maapi.run_with_retry(vrp_step1, commit_params=commit_params)

        elif 'http://xml.juniper.net/xnm/1.1/xnm' in capas \
             and ncs_device.platform.version \
             and int(ncs_device.platform.version.split('.')[0]) < 17:
            # check if tricks were previously performed on this device?
            if ncs_device.config.junos__configuration.system.services.netconf.rfc_compliant.exists():
                self.log.debug('tricks already performed on juniper-junos, skipping...')
                yield WorkStepOutput.ok()
                return
            yield WorkStepOutput.work()

            def junos_step1(t_write):
                write_root = ncs.maagic.get_root(t_write)
                write_ncs_device = write_root.devices.device[device]
                write_ncs_device.config.junos__configuration.system.services.netconf.rfc_compliant.create()
                return True

            try:
                maapi.run_with_retry(junos_step1, commit_params=commit_params)
            except _ncs.error.Error as e:
                if 'does not match the start tag' in str(e) or "'nc' is an unknown prefix" in str(e):
                    self.log.debug(f'Error "{e}" setting rfc-compliant as expected, performing sync-from')
                    ncs_device.sync_from()
                else:
                    raise e

        yield WorkStepOutput.ok()


    def _setup_management_credentials(self, root, service, maapi) -> Iterator[WorkStepOutput]:
        """Setup the management credentials on the device
        """
        device = service.device

        # check if the preferred management credentials are configured for the device authgroup
        dcj = device_credentials.DeviceCredentialJuggler(root, maapi, self.log)
        credentials = next(dcj.collect_credentials(device))
        config_correct = dcj.check_configured_credentials(device, credentials, service.management_credentials.create_management_credentials)

        # check if final management credentials are in use and actually work
        if config_correct:
            success, _ = dcj.test_credentials(service, root.devices.device[device])
            if success:
                yield WorkStepOutput.ok()
                return

        # by now we know the management credentials are either not configured or do not work
        if not service.management_credentials.create_management_credentials:
            yield WorkStepOutput.fail('Automaton is not allowed to create management credentials and the ones provided do not work')
            return

        yield WorkStepOutput.work()

        dcj = device_credentials.DeviceCredentialJuggler(root, maapi, self.log)
        success, error = dcj.setup_management_device_credentials(device)
        if success:
            yield WorkStepOutput.ok()
        else:
            yield WorkStepOutput.fail(error)


class DeleteDeviceAutomatonAction(ncs.dp.Action, classes.Retrier):
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        self.log.info('Action {} start'.format(name))
        with utils.maapi_session('python-devaut-delete-device-automaton', 'system') as m:
            # delete operations may fail due to commit queue asynchronous processing, use _retry_function
            device = action_input.name
            result = self._retry_function(self._delete_device, args=[m, device, f'{kp}/devaut:automaton{{{device}}}', uinfo],
                                            suffix='delete-device on ' + device)

            if not result[0]:
                self.log.error('{}: action error {}'.format(name, result[1]))
                action_output.success = False
                action_output.message = result[1]

        self.log.info('Action {} finished'.format(name))

    def _delete_device(self, maapi, device, kp, uinfo):
        device_xpath = f'/ncs:devices/ncs:device[ncs:name="{device}"]'
        self.log.debug(f'{kp}: deleting device, acquiring partial lock on {device_xpath}')

        # It is critical that other users do not attempt to "use" the device as
        # it is being removed. In particular the race condition between
        # deleting the device list entry (this function) and creating the
        # device list entry (pre-modification in automaton) may occur if an
        # automaton service is deleted and then quickly recreated with the same
        # name.
        with MaapiLocker(maapi).lock(device_xpath, timeout=120) as (success, reason):
            if not success:
                self.log.error(f'{kp}: error acquiring partial lock {device_xpath}: {reason}')
                return
                # retry?!

            # first, set device admin-state to southbound-locked. this will abort any commit queue items currently
            # executing towards the device and ensure commit queue prune will actually remove the device from the queue
            with maapi.start_write_trans() as t_write:
                self.log.debug(f'{kp}: setting admin-state=southbound-locked on device')
                root_write = ncs.maagic.get_root(t_write)
                try:
                    # write operation may fail if the device list entry is gone
                    root_write.devices.device[device].state.admin_state = 'southbound-locked'
                    t_write.apply()
                except KeyError as ke:
                    self.log.warning(f'{kp}: error setting admin-state=southbound-locked - device already removed: {ke}')
                    return

            # prune device from any commit queue items
            self.log.debug(f'{kp}: starting commit queue pruning')
            try:
                utils.prune_device_from_queue_items(log=self.log, maapi=maapi, device=device, uinfo=uinfo)
            except KeyError as ke:
                self.log.warning(f'{kp}: error commit queue pruning - device already removed: {ke}')
                return

            with maapi.start_write_trans() as t_write:
                # Remove the /devices/device list entry for this device. Delete
                # operations are safe (noop) if the device list entry is already
                # gone.
                root_write = ncs.maagic.get_root(t_write)
                del root_write.devices.device[device]
                # Remove device-specific authgroup for this device
                del root_write.devices.authgroups.devaut__authgroup[device]
                t_write.apply()

            # Clear any outstanding alarms related to the device. These could
            # be alarms managed by the automaton (like connectivity related) or
            # alarms managed by NSO but for whatever reason not cleared when
            # the device was removed. Right now NSO *does not* clear its own
            # outstanding device for a removed device. Maybe this will change
            # in future releases? Let's hope the operation is synchronous with
            # deleting the device, otherwise this is a potential race condition
            # in the making :)
            self.log.debug(f'{kp}: removing outstanding device alarms')
            device_alarm.clear_device_alarms(maapi, device)


class DeviceCheckSyncAction(ncs.dp.Action):
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        self.log.info('Action {} start'.format(name))
        with utils.maapi_session(uinfo.username, uinfo.context) as m:
            t_read = m.attach(uinfo.actx_thandle)
            root = ncs.maagic.get_root(t_read)
            device = str(kp[0][0])
            device_automaton = root.ncs__devices.devaut__automaton[device]

            dcs = DeviceCheckSync(m, self.log)
            results = dcs.device_check_sync([device], uinfo)

            action_output.success = results[device].sync_result == 'in-sync'

            dcs.results_to_cdb_state(results)
            if device_automaton.consistency_guarantor:
                dcs.results_to_reaction(results)

            maagic_copy(root.ncs__devices.devaut__automaton[device].device_check_sync_state,
                        action_output.check_sync_result)

            m.detach(uinfo.actx_thandle)
        self.log.info('Action {} finished'.format(name))


class DevicesCheckSyncAction(ncs.dp.Action):
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        self.log.info('Action {} start'.format(name))
        with utils.maapi_session(uinfo.username, uinfo.context) as m:
            t_read = m.attach(uinfo.actx_thandle)
            root = ncs.maagic.get_root(t_read)

            dcs = DeviceCheckSync(m, self.log)
            results = dcs.device_check_sync(self._get_devices(root), uinfo)

            action_output.success = True

            dcs.results_to_cdb_state(results)
            dcs.results_to_reaction(results)

            for device_name in results.keys():
                r = action_output.device.create()
                r.name = device_name

                maagic_copy(root.ncs__devices.devaut__automaton[device_name].device_check_sync_state,
                            r.check_sync_result)

            m.detach(uinfo.actx_thandle)
        self.log.info('Action {} finished'.format(name))

    def _get_devices(self, root):
        for device in root.ncs__devices.devaut__automaton:
            if device.plan.component['self'].state['ncs:ready'].status == 'reached':
                yield device.device


class ReactAlarmAction(ncs.dp.Action):
    """React to device alarms by checking if still relevant, and enqueueing the work function.
    """
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        log = classes.LoggerWrapper(self.log, f'Action {kp}/{name}: {action_input.path}')
        with utils.maapi_session('device-react-alarm-connection-failure-action', 'system') as m:
            with m.start_read_trans() as t_read:
                # get the alarm that triggered us
                try:
                    al = ncs.maagic.get_node(t_read, action_input.path)
                except KeyError:
                    # alarm has apparently gone away, or was never there
                    # (someone invoked us manually etc)
                    log.debug("Alarm does not exist, returning")
                    return

                if al.type not in ('al:connection-failure', 'al:out-of-sync', 'al:commit-through-queue-failed'):
                    log.info(f'Unknown alarm type {al.type}')
                    return

                # no point in doing anything if alarm has been cleared
                if al.is_cleared:
                    log.debug("Alarm is already cleared, returning")
                    return

                alarm_type = al.type.replace('-', '_').replace('al:', '')
                read_service = ncs.maagic.get_node(t_read, kp)
                interesting = getattr(read_service.alarm_reaction, alarm_type)
                if not interesting:
                    self.log.info(f'Reactions to alarm {al.type} are disabled')
                    return

                last_alarm_text = al.last_alarm_text

                # cache the alarm_id object if we need to handle the alarm later
                alarm_id = alarm_sink.AlarmId(al.device, al.managed_object, al.type, al.specific_problem)

            if alarm_id.type == 'al:commit-through-queue-failed' and is_error_transient(last_alarm_text):
                ask = alarm_sink.AlarmSink(m)
                alarm = alarm_sink.Alarm(alarm_id, severity=alarm_sink.PerceivedSeverity.CLEARED,
                        alarm_text='Transient error detected, attempting automatic fix with device automaton')
                alarm.cleared = True
                ask.submit_alarm(alarm)
                self.log.debug(f'Cleared alarm {alarm_id} because it looks like a transient error')
            with m.start_write_trans(db=ncs.OPERATIONAL) as t_write:
                write_service = ncs.maagic.get_node(t_write, kp)
                log_message = f'Reacted to {al.type} alarm: {last_alarm_text}'
                service_log.log(write_service, entry_type='devaut:service-state-changed',
                                level='info', message=log_message)
                t_write.apply()

                # copy the kicker:action-input-params data from this action to enqueue_work
                enqueue_input = write_service.enqueue_work.get_input()
                maagic_copy(action_input, enqueue_input)
                write_service.enqueue_work(enqueue_input)


class ServiceApp(ncs.application.Application):
    __workers: List[classes.SupportsStartStop] = []

    def setup(self):
        self.register_service('devaut-servicepoint', DeviceAutomaton)
        self.register_action('devaut-delete-device-automaton-actionpoint', DeleteDeviceAutomatonAction)
        self.register_action('devaut-check-sync-actionpoint', DeviceCheckSyncAction)

        # The new bgworker require slightly different initialization.
        bg_workers = (
            (device_automaton_retrier.retry_device_automatons,
             '/ncs:devices/global-settings/devaut:automaton/retrier/enabled'),
            (device_config_consistency_guarantor.run_device_ccg,
             '/ncs:devices/global-settings/devaut:automaton/configuration-consistency-guarantor/enabled'),
            (DeviceMonitorWorker(),
             '/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:enabled')
        )
        for proc, conf_node in bg_workers:
            # mypy does not recognize the DeviceMonitorWorker() instance as callable, but I know it is ...
            worker = background_process.Process(self, proc, config_path=conf_node) # type: ignore
            worker.start()
            self.__workers.append(worker)

        # the thread based workers
        for worker_cls in (PostCommitSubscriber,
                           PostCommitOperPlanSubscriber,
                           ClearMepAlarmSubscriber):
            # mypy does not recognize the base Subscriber class to contain the start/stop methods.
            # These subscribers will be replaced with kickers soon anyway ...
            thread_worker: classes.SupportsStartStop = worker_cls(app=self, log=self.log) # type: ignore
            thread_worker.start()
            self.__workers.append(thread_worker)

    def teardown(self):
        for worker in self.__workers:
            worker.stop()


class WorkActionApp(ncs.application.Application):
    def setup(self):
        self.register_action('devaut-work-actionpoint', WorkAction)


class EnqueueWorkActionApp(ncs.application.Application):
    def setup(self):
        # Reset work-count to zero
        # this ensures that when Python VM restarts, we do not have any
        # dangling job from previous run.
        with ncs.maapi.single_write_trans('python-devaut-enqueue-work-cleanup', 'system', db=ncs.OPERATIONAL) as oper_t_write:
            root = ncs.maagic.get_root(oper_t_write)
            for device in root.ncs__devices.devaut__automaton:
                device.work_count = 0
            oper_t_write.apply()

        self.register_action('devaut-enqueue-work-actionpoint', EnqueueWorkAction)


class DevicesCheckSyncActionApp(ncs.application.Application):
    def setup(self):
        self.register_action('devaut-devices-check-sync-actionpoint', DevicesCheckSyncAction)


class ReactAlarmActionApp(ncs.application.Application):
    def setup(self):
        self.register_action('devaut-react-alarm-actionpoint', ReactAlarmAction)
