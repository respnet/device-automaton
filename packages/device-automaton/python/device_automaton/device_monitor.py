import logging
import time
import traceback
from collections import OrderedDict
from dataclasses import dataclass
from datetime import datetime, timedelta
from io import StringIO
import socket
from typing import Dict, Iterable, Optional, Tuple

import paramiko
from alarm_sink import alarm_sink
from maagic_copy.maagic_copy import maagic_copy
import ncs

from . import device_alarm, device_credentials, device_management_endpoint, enqueue_work
from .common import classes, utils
from .futurama import concurrent_worker
from .ned_id import NedInfo
from .nso6_trans_conflict_retry import *


@dataclass(frozen=True)
class Device:
    name: str
    management_endpoint: Tuple[device_management_endpoint.ManagementEndpoint, ...]
    burst_limit: bool
    credentials: Optional[device_credentials.DeviceCredentials]
    keypath: str
    # whether to check for SSH connection for this device, if false only
    # check for TCP connection
    ssh_check: bool

    def __str__(self):
        return '{} management-endpoint={}'.format(self.keypath, self.management_endpoint)


@dataclass
class DeviceMonitorResult:
    """Data class for results returned by the device monitor"""

    management_endpoint: Optional[device_management_endpoint.ManagementEndpoint] = None
    state_changed = False
    updated = False
    ssh_host_key_fetch_needed = False


@dataclass(frozen=True)
class _SshAliveResult:
    """Internal DTO for passing results from DeviceMonitor.is_ssh_alive"""
    alive: bool
    poll_start: datetime
    poll_end: datetime


_TAliveResult = Dict[device_management_endpoint.ManagementEndpoint, _SshAliveResult]

# paramiko logs DEBUG by default, which is super verbose
# Setting to Critical and let our code handle the exceptions
logging.getLogger('paramiko.transport').setLevel(logging.CRITICAL)


class DeviceMonitor(classes.DeviceTimeoutTaker):
    def __init__(self, log, maapi: ncs.maapi.Maapi, verbose=False, history_size=50) -> None:
        self.log = log
        self._maapi = maapi
        self._verbose = verbose
        self._max_retries = 3
        self._history_size = history_size

    def is_ssh_alive(self, device: Device) -> _TAliveResult:
        """"Worker" method for checking SSH aliveness on a device

        This code must be thread-safe, as it may be execute in parallel for
        multiple devices.
        Uses paramiko to connect to device management endpoint with SSH, and
        returns the results for all management endpoints in a dict:

        {'mep1': _SshAliveResult(alive=True,
           poll_start=datetime(2018, 1, 15, 8, 58, 19, 184559),
           poll_end=datetime(2018, 1, 15, 8, 59, 00, 123456)}

        :param device: Device dataclass instance
        :return: a dict of results for all management endpoints"""

        results: _TAliveResult = OrderedDict()
        for management_endpoint in device.management_endpoint:
            poll_start = datetime.utcnow()
            path = '{}/management-endpoint{{{}}}'\
                .format(device.keypath, management_endpoint.address)
            if device.burst_limit:
                time.sleep(2)
            with paramiko.SSHClient() as ssh:
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                alive = False
                try:
                    if not device.ssh_check:
                        #  check only network connections
                        self.log.debug(f'skip-ssh-auth-check for {management_endpoint.address} port {management_endpoint.port}')
                        with socket.create_connection((management_endpoint.address, management_endpoint.port)) as s:
                            self.log.debug(f'connected to {management_endpoint.address}')
                        alive = True
                    elif device.credentials:
                        if self._verbose:
                            self.log.debug('{}: is_ssh_alive testing with credentials {}'.format(path, device.credentials))
                        if device.credentials.private_key:
                            private_key_temp = StringIO(device.credentials.private_key)
                            private_key_ready: paramiko.pkey.PKey
                            match device.credentials.algorithm:
                                case device_credentials.SshKeyAlgorithm.SSH_RSA:
                                    private_key_ready = paramiko.RSAKey.from_private_key(private_key_temp)
                                case device_credentials.SshKeyAlgorithm.ECDSA_SHA2_NISTP256:
                                    private_key_ready = paramiko.ECDSAKey.from_private_key(private_key_temp)
                                case device_credentials.SshKeyAlgorithm.SSH_ED25519:
                                    private_key_ready = paramiko.Ed25519Key.from_private_key(private_key_temp)
                                case _:
                                    raise ValueError(f'Unsupported SSH key type: {device.credentials.algorithm}')
                            ssh.connect(management_endpoint.address, management_endpoint.port, pkey=private_key_ready,
                                    username=device.credentials.username, timeout=5, banner_timeout=5,
                                    look_for_keys=False)
                        elif device.credentials.password:
                            ssh.connect(management_endpoint.address, management_endpoint.port,
                                    username=device.credentials.username,
                                    password=device.credentials.password, timeout=5, banner_timeout=5,
                                    look_for_keys=False)
                    else:
                        if self._verbose:
                            self.log.debug('{}: is_ssh_alive testing with connect'.format(path))
                        ssh.connect(management_endpoint.address, management_endpoint.port, timeout=5, banner_timeout=5,
                                    look_for_keys=False)
                        # ssh.connect is asynchronous here, we need to wait >banner_timeout
                        # to catch a potential exception
                        time.sleep(6)
                except paramiko.ssh_exception.AuthenticationException as e:
                    # maybe we used incorrect credentials? doesn't matter, endpoint is still alive
                    if self._verbose:
                        self.log.debug('{}: exception in is_ssh_alive - {}: {}'
                                       .format(path, type(e).__name__, e))
                    alive = True
                except Exception as e:
                    if self._verbose:
                        self.log.info('{}: exception in is_ssh_alive - {}: {}'
                                      .format(path, type(e).__name__, e))
                    if str(e) == 'No authentication methods available' and device.credentials is None:
                        # we didn't provide any credentials, but connected successfuly (normal path
                        # for monitoring without credentials)
                        alive = True
                else:
                    # if we reached here, we were able to authenticate (normal path for monitoring
                    # with credentials)
                    if device.credentials:
                        alive = True

            poll_end = datetime.utcnow()
            results[management_endpoint] = _SshAliveResult(alive, poll_start, poll_end)
            self.log.debug('{} is_ssh_alive: test with {} alive={}, duration={}s'
                           .format(path, 'credentials ' + str(device.credentials) if device.credentials else 'connect', alive,
                                   (poll_end - poll_start).total_seconds()))
        return results

    def evaluate_ssh_alive_results(self, device: Device,
                                   results: _TAliveResult, retry_count: int) \
            -> Tuple[Optional[device_management_endpoint.ManagementEndpoint], bool]:
        """Evaluate the results of is_ssh_alive & write to CDB

        :param device: the Device object
        :param results: the results dict, created by is_ssh_alive - ordered management endpoints with results
        :param retry_count: current retry count
        :return: tuple of (result, should_retry)

        This method will write operational data to CDB for management endpoints.
        It will also raise/clear alarms for individual unreachable management endpoints.

        The return dict has the following structure:
        { Device(name='dut-r1', management_endpoint=...):
          Optional[ManagementEndpoint(address=...)] }
        """
        self.log.debug('{} evaluate_ssh_alive_results retry_count={}'.format(device.keypath, retry_count))
        if self._verbose:
            self.log.debug('{}: results {}'.format(device.keypath, results))

        with alarm_sink.AlarmSink() as ask:
            with self._maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t:

                oper_root = ncs.maagic.get_root(oper_t)
                try:
                    oper_service = oper_root.ncs__devices.devaut__automaton[device.name]
                except KeyError:
                    # maybe device automaton instance was removed?
                    # alarms will be cleared by ClearMepAlarmsReaction
                    pass
                else:
                    for mep, mep_res in results.items():
                        try:
                            current_status = oper_service.management_endpoint[mep.address].alive
                            oper_ep = oper_service.management_endpoint[mep.address]
                            oper_ep.alive = mep_res.alive
                            oper_ep.last_update = utils.format_yang_date_and_time(mep_res.poll_end)
                            changed = current_status != mep_res.alive
                            if changed:
                                # only submit/clear alarm on state change
                                self.log.debug('submitting alarm status change for device {} with alive = {}'.format(device.name, mep_res.alive))
                                device_alarm \
                                    .submit_device_automaton_alarm(ask, device.name,
                                                                 failed=not mep_res.alive,
                                                                 management_endpoint=mep.address)
                                oper_ep.last_transition = utils.format_yang_date_and_time(mep_res.poll_end)

                            # write the poll stats
                            poll_start = utils.format_yang_date_and_time(mep_res.poll_start)
                            poll_end = utils.format_yang_date_and_time(mep_res.poll_end)
                            poll_stats = oper_ep.poll_stats.create(poll_start)
                            poll_stats.end_timestamp = poll_end
                            poll_stats.duration = int((mep_res.poll_end - mep_res.poll_start).total_seconds() * 100)
                            poll_stats.changed = changed
                            poll_stats.alive = mep_res.alive

                            # prune old entries
                            poll_stats_keys = oper_ep.poll_stats.keys()
                            if len(poll_stats_keys) > self._history_size:
                                for poll_stat in poll_stats_keys[:len(poll_stats_keys) - self._history_size]:
                                    del oper_ep.poll_stats[poll_stat]

                        except KeyError:
                            # maybe management endpoint was removed?
                            # alarms will be cleared by ClearMepAlarmsReaction
                            pass
                    oper_t.apply()

        management_endpoint = next((mep for mep, mep_res in results.items() if mep_res.alive), None)
        if management_endpoint:
            self.log.debug('{} evaluate_ssh_alive_results finish, device has active management-endpoint {}'
                           .format(device.keypath, management_endpoint.address))
            return management_endpoint, False
        else:
            tried_meps = ', '.join('{}:{}'.format(mep.address, mep.port) for mep in device.management_endpoint)
            self.log.debug('{} evaluate_ssh_alive_results failed, device has no active management-endpoint (tried: {})'
                           .format(device.keypath, tried_meps))
            return None, retry_count < self._max_retries

    def error_cb(self, device: Device, exception: Exception, retry_count: int):
        """Evaluate an exception ocurred in is_ssh_alive and return retry info"""
        self.log.error('{}: error {}'.format(device.keypath, exception))
        return None, retry_count < self._max_retries

    def monitor(self, device_filter=None, max_workers=5, max_retries=3):
        """Main entry point into device monitor

        Will use concurrent_worker to process devices in parallel. Returns a
        dictionary of Device objects (key) and active management endpoints (value).

        :param device_filter: optional filter for device automaton instances to poll
        :param max_workers: max threads for SSH poller
        :param max_retries: max retries for failed SSH polls for a single device
        :return: dict of {Device_object: management_endpoint} results"""
        self._max_retries = max_retries
        self.log.info('starting monitor')
        result = concurrent_worker(items=self._get_devices(device_filter),
                                   work_fn=self.is_ssh_alive,
                                   done_fn=self.evaluate_ssh_alive_results,
                                   error_fn=self.error_cb,
                                   log=self.log,
                                   max_workers=max_workers,
                                   verbose=self._verbose)
        self.log.info('finished monitor')
        return result

    @classmethod
    def _support_ssh(cls, ned_id: str, virtual_router: bool) -> bool:
        """Checks whether the NED supports SSH
        :param ned_id: device ned-id
        :return: false for generic NED (unless virtual-router leaf is set), true for others
        """
        ned_info = NedInfo.from_cdb(ned_id)
        return ned_info.generic is False or ned_info.generic is True and virtual_router is True

    def _get_devices(self, device_filter=None) -> Iterable[Device]:
        """Generator for devices that need to be polled

        The resulting items use the Device dataclass to encapsulate all the
        data needed for the monitoring function.

        :param device_filter: optional device filter function. Will get device
            service instance as input and must return a boolean
        :return: generator of Device objects"""
        with self._maapi.start_read_trans() as t:
            root = ncs.maagic.get_root(t)
            for device in root.ncs__devices.devaut__automaton:
                if callable(device_filter):
                    if not device_filter(device):
                        continue

                try:
                    meps = list(device_management_endpoint.from_device_automaton(device))
                    if device.plan.component[device.device].state['devaut:initial-credentials-configured'].status == 'reached':
                        device_credentials_tuple = device_credentials.get_device_credentials(root, device.device)
                    else:
                        device_credentials_tuple = None
                    # Cisco XR SSH rate limiter doesn't handle bursts
                    burst_limit = 'http://cisco.com/ns/yang/cisco-xr-types' in root.ncs__devices.ncs__device[device.device].capability
                    yield Device(device.device, tuple(meps),
                                 burst_limit,
                                 device_credentials_tuple,
                                 device._path,
                                 DeviceMonitor._support_ssh(device.ned_id, device.virtual_router))
                except Exception as e:
                    if classes.device_entry_removed(e):
                        self.log.info(f'/devices/device{{{device.device}}} list entry missing, re-deploying the automaton service instance')
                        device.reactive_re_deploy()
                    else:
                        self.log.error('Failed to add device {} to monitored list, error {}'.format(device.device, e))
                        self.log.error(traceback.format_exc())

    @retry_on_conflict()
    def reconfigure_device_management_endpoint(self, device=None) \
            -> Dict[str, DeviceMonitorResult]:
        """Reconfigure recovered devices management endpoints & handle alarms

        Will iterate through all device automaton instances.

        If the device is alive, possibly reconfigure management endpoint. If it
        has changed, perform a SSH host key update. If the device has recovered
        from a failure, clear the device automaton alarm.

        If the device is dead, raise a device automaton alarm.

        :param device: optional target device name
        :return: a dict of {device_name: DeviceMonitorResult() }"""
        results = {}
        self.log.debug('management-endpoint update start')
        with self._maapi.start_write_trans() as t_write:
            root = ncs.maagic.get_root(t_write)
            if device:
                devices = iter([root.ncs__devices.devaut__automaton[device]])
            else:
                devices = iter(root.ncs__devices.devaut__automaton)

            for service in devices:
                device_result = DeviceMonitorResult()
                results[service.device] = device_result

                management_endpoint = next((mep for mep in service.management_endpoint if mep.alive), None)
                if not management_endpoint:
                    self.log.warning(f'{service._path}: Device has no reachable management endpoint!')
                    device_result.state_changed = (service.alive is not False)
                    service.alive = False
                    service.ssh_host_key_fetch_needed = False
                    if device_result.state_changed:
                        service.last_transition = utils.format_yang_date_and_time()
                    continue

                device_result.state_changed = (service.alive is not True)
                if device_result.state_changed:
                    service.last_transition = utils.format_yang_date_and_time()
                service.alive = True
                device_result.management_endpoint = \
                    device_management_endpoint.ManagementEndpoint(management_endpoint.address, management_endpoint.port)

                if service.wait_to_change_management_endpoint is not None:
                    wait_to_change_mep = timedelta(seconds=service.wait_to_change_management_endpoint)
                else:
                    wait_to_change_mep = timedelta(seconds=root.ncs__devices.global_settings\
                            .devaut__automaton.device_monitor.wait_to_change_management_endpoint)
                last_transition = utils.convert_yang_date_and_time_to_datetime(management_endpoint.last_transition)
                if last_transition:
                    mep_active_long_enough = (last_transition + wait_to_change_mep) < datetime.utcnow()
                else:
                    mep_active_long_enough = False
                cdb_device = root.devices.device[service.device]
                mep_changed = cdb_device.address != management_endpoint.address \
                    or cdb_device.port != management_endpoint.port

                if mep_changed:
                    if not mep_active_long_enough:
                        self.log.debug(f'{service._path}: Management endpoint {device_result.management_endpoint} is not active longer than {wait_to_change_mep}, will not change')
                        continue
                    else:
                        cdb_device.address = management_endpoint.address
                        cdb_device.port = management_endpoint.port
                        self.log.info(f'{service._path}: Management endpoint changed to {device_result.management_endpoint}')
                        device_result.updated = True
                else:
                    self.log.debug(f'{service._path}: Management endpoint unchanged')

                if not DeviceMonitor._support_ssh(service.ned_id, service.virtual_router):
                    service.ssh_host_key_fetch_needed = False
                elif device_result.updated or len(cdb_device.ssh.host_key) == 0:
                    device_result.ssh_host_key_fetch_needed = True
                    service.ssh_host_key_fetch_needed = True
                else:
                    service.ssh_host_key_fetch_needed = False

            self.log.debug('management-endpoint update apply transaction')
            t_write.apply()
        self.log.debug('management-endpoint update finish')

        # Handle alarms:
        #   - raise alarms for devices that just failed monitoring:
        #       (all management endpoints down) and (plan status still not shown as failed)
        #   - clear alarms for devices that just recovered:
        #       (some management endpoints up) and (plan status still shown as failed)
        self.log.debug('alarm update start')
        with alarm_sink.AlarmSink() as ask:
            for dev, result in results.items():
                if not result.management_endpoint and result.state_changed:
                    self.log.debug('set device alarm for device ', dev)
                    device_alarm.submit_device_automaton_alarm(ask, dev, failed=True)
                elif result.management_endpoint and result.state_changed:
                    self.log.debug('clear device alarm for device ', dev)
                    device_alarm.submit_device_automaton_alarm(ask, dev, failed=False)
                else:
                    self.log.debug('no change for device alarm for device ', dev)
        self.log.debug('alarm update finish')

        return results

    def kick_updated_service_instances(self, device_results: Dict[str, DeviceMonitorResult]):
        for device, results in device_results.items():
            if results.state_changed or results.ssh_host_key_fetch_needed:
                path = '/ncs:devices/devaut:automaton{{{}}}'.format(device)
                try:
                    service = ncs.maagic.get_node(self._maapi, path)
                    enqueue_work.action(service, enqueue_work.InternalSource.MONITOR)
                except Exception as e:
                    self.log.error('Failed to execute {}, error {}: {}'.format(path, type(e).__name__, e))
                    self.log.error(traceback.format_exc())


class DeviceMonitorWorker:
    def __init__(self):
        # set the default settings (in essence, to shut up pylint)
        self._polling_cycle = 300
        self._max_workers = 5
        self._max_retries = 3
        self._enabled = True
        self._verbose = False
        self._device_poll_history_size = 50
        self._global_poll_history_size = 50

    def write_stats(self, stats, results):
        """Write statistics for the last monitoring cycle and raise an alarm if the poll duration
        is longer than the configured monitoring period
        """
        duration = (stats['end'] - stats['start']).total_seconds()
        with self._maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t:
            root = ncs.maagic.get_root(oper_t)
            lps = root.ncs__devices.global_settings.devaut__automaton.device_monitor.last_poll_stats
            poll_start = utils.format_yang_date_and_time(stats['start'])
            lps.start_timestamp = poll_start
            lps.end_timestamp = utils.format_yang_date_and_time(stats['end'])
            lps.total_duration = int(duration * 100)
            lps.polling_duration = int(stats['fetch-device-data-duration'] * 100)
            lps.update_mep_duration = int(stats['update-mep-duration'] * 100)
            lps.update_state_duration = int(stats['update-state-duration'] * 100)
            lps.total_devices = len(results)
            lps.total_meps = sum([len(device.management_endpoint) for device in results.keys()])
            lps.unreachable_devices = len([device for device, mep in results.items() if mep is None])

            poll_stats = root.ncs__devices.global_settings.devaut__automaton.device_monitor.poll_stats_history.poll_stats
            poll_stats_history_entry = poll_stats.create(poll_start)
            maagic_copy(lps, poll_stats_history_entry)

            # prune old poll stat history entries
            poll_stats_keys = poll_stats.keys()
            if len(poll_stats_keys) > self._global_poll_history_size:
                for poll_stat in poll_stats_keys[:len(poll_stats_keys) - self._global_poll_history_size]:
                    del poll_stats[poll_stat]

            oper_t.apply()

        # now check if we need to raise/clear alarms
        alarm_threshold = max(self._polling_cycle + 10, self._polling_cycle*1.1)
        raise_alarm = duration >= alarm_threshold
        clear_alarm = duration <= self._polling_cycle

        if raise_alarm:
            self._log.debug(f'Submitting alarm since duration {duration} is higher than threshold {alarm_threshold}')
            alarm_text = f'Device monitor polling cycle took {duration:.1f} seconds, {duration - self._polling_cycle:.1f} longer than the configured polling period'
        elif clear_alarm:
            self._log.debug(f'Clearing alarm since duration {duration} is under {self._polling_cycle}')
            alarm_text = 'Device monitor polling cycle duration is within the configured monitoring period'
        else:
            self._log.debug(f'No change for alarm since duration {duration} is between {self._polling_cycle} and {alarm_threshold}')
            return

        alarm_device = 'ncs'
        alarm_type = 'devaut:device-monitor-duration-alarm'
        alarm_mo = '/ncs:devices/global-settings/devaut:automaton/device-monitor'
        alarm_id = alarm_sink.AlarmId(device=alarm_device,
                                      managed_object=alarm_mo,
                                      type=alarm_type,
                                      specific_problem=None)

        with alarm_sink.AlarmSink() as ask:
            alarm = alarm_sink.Alarm(alarm_id,
                                     severity=alarm_sink.PerceivedSeverity.MINOR,
                                     alarm_text=alarm_text)
            alarm.cleared = not raise_alarm
            ask.submit_alarm(alarm)

    def __call__(self):
        # This method will be executed by bgworker in a separate process, so
        # this is where we must initialize resources we need
        self._log = ncs.log.Log(logging.getLogger('device-monitor'))
        self._maapi = ncs.maapi.Maapi()
        self._maapi.start_user_session('python-devaut-monitor', 'system')
        self._log.info('worker running')

        # initial backoff period to make sure everything was configured in NSO (CI)
        timeout: float = 30

        while True:
            try:
                self._read_settings()
                start = datetime.utcnow()
                dm = DeviceMonitor(maapi=self._maapi, log=self._log, verbose=self._verbose, history_size=self._device_poll_history_size)
                results = dm.monitor(max_workers = self._max_workers, max_retries=self._max_retries)
                end_fetch_device_data = datetime.utcnow()

                # Kick the failed service instances for devices that have a reachable
                # management endpoint. If they're not alive, there is no point so we exclude failed
                start_update_mep = datetime.utcnow()
                reconfigure_results = dm.reconfigure_device_management_endpoint()
                end_update_mep = datetime.utcnow()
                dm.kick_updated_service_instances(reconfigure_results)
                end_update_state = datetime.utcnow()

                end = datetime.utcnow()
                duration = (end - start).total_seconds()
                self._log.debug('monitor finished, took {} seconds'.format(duration))
                stats = {'start': start,
                         'end': end,
                         'fetch-device-data-duration': (end_fetch_device_data - start).total_seconds(),
                         'update-mep-duration': (end_update_mep - start_update_mep).total_seconds(),
                         'update-state-duration': (end_update_state - end_update_mep).total_seconds()}
                self.write_stats(stats, results)

                timeout = self._polling_cycle - duration if duration < self._polling_cycle else 0

                self._log.debug('going to wait for {} seconds'.format(timeout))
                time.sleep(timeout)
            except Exception as e:
                self._log.error('error in worker', e)
                self._log.error(traceback.format_exc())

    def _read_settings(self):
        with self._maapi.start_read_trans() as t:
            self._polling_cycle = t.get_elem('/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:monitoring-period').as_pyval()
            self._max_workers = t.get_elem('/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:max-workers').as_pyval()
            self._max_retries = t.get_elem('/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:max-retries').as_pyval()
            self._enabled = t.get_elem('/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:enabled').as_pyval()
            self._verbose = t.get_elem('/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:verbose').as_pyval()
            self._device_poll_history_size = t.get_elem('/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:device-poll-history-size').as_pyval()
            self._global_poll_history_size = t.get_elem('/ncs:devices/global-settings/devaut:automaton/devaut:device-monitor/devaut:global-poll-history-size').as_pyval()


if __name__ == '__main__':
    class _Log():
        def __getattr__(self, item):
            return lambda x, *xx: print('{}: {} {}'.format(datetime.utcnow().isoformat(), item.upper(), x), *xx)

    with utils.maapi_session('python-devaut-monitor-test', 'system') as m:
        d = DeviceMonitor(_Log(), m)
        print(d.monitor())
