from dataclasses import dataclass
from typing import Iterator, Optional

from . import ned_id


@dataclass(frozen=True)
class ManagementEndpoint:
    address: str
    port: int

    def __str__(self):
        return '({}, {})'.format(self.address, self.port)


def from_device_automaton(service) -> Iterator[ManagementEndpoint]:
    ned_info = ned_id.NedInfo.from_cdb(service.ned_id)
    netconf = ned_info.netconf

    for mep in service.management_endpoint:
        if mep.port is not None:
            port = mep.port
        elif netconf:
            port = 830
        else:
            port = 22
        yield ManagementEndpoint(mep.address, port)
